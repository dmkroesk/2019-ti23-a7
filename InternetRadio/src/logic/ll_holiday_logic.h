/*
 *  ll_holiday_logic.h
 *
 *  Created: 21-Mar-19 12:00:00 AM
 *  Author: Marck
 */
#ifndef LL_HOLIDAY_LOGIC_H_
#define LL_HOLIDAY_LOGIC_H_

extern void init_holiday(void);
extern void update(int tickrate);
extern void check_for_holidays(int year, int month, int day);
extern void check_the_json_for_holiday(char *json_holiday);
extern int check_for_valid_json(char *json);
extern int retrieve_holiday_name(char *name, char *json);
extern int retrieve_status_code(char *status_string, char *json);
extern char * get_todays_holiday(void);

#endif /* LL_HOLIDAY_LOGIC_H_ */

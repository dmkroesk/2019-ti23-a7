/*
 * ll_clock_logic.c
 *
 * Created: 26-Feb-19 11:10:05 AM
 *  Author: Gerben, Matthijs & Marck
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "driver/dl_rtc_driver.h"
#include "driver/dl_audio_driver.h"
#include "driver/dl_storage_driver.h"

#include "logic/ll_clock_logic.h"
#include "logic/ll_menu_logic.h"
#include "logic/ll_inet_logic.h"

//refresh rate of the alarm loop, refresh every second
#define ALARM_TICK_RATE 1000

int tick_count = 0;

//number of days in week. counted from 0(Sunday)
const int DAYS = 7;

int *current_alarm_status = NULL;

alarm_struct **alarms;
alarm_struct *temp_alarm;
//alarm_struct *my_alarm;

void print_alarm_by_index(int alarm);

void print_alarm(alarm_struct *alarm);
void trigger_alarm(alarm_struct *alarm);
void skip_a_day(void);
int retrieve_is_beep(void);
void set_alarm(int hour, int minute);
void get_alarms_from_storage(void);

void test_alarm(void);

//initializes all alarms. This should be called in Main before alarms can be used.
void init_alarms()
{
	alarms = malloc(sizeof(alarm_struct *) * 7);
	
	get_alarms_from_storage();
}

void get_alarms_from_storage()
{
	settings *setting = malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	int i = 1;
	for(; i< DAYS; i++)
	{
		alarms[i] = &setting->alarm[0];
		setting->alarm[i] = setting->alarm[0];
	}
	write_setting(setting, sizeof(settings));
}

//Test method for the alarm
void set_alarm_final(int hour, int minute)
{
	int i = 0;
	for(; i< DAYS; i++)
	{
		alarms[i]->hour_begin = hour;
		alarms[i]->hour_end = hour;
		alarms[i]->minute_begin = minute;
		alarms[i]->minute_end = minute + 3;
	}
}

//creates a alarm struct with a begin- and endtime
alarm_struct *create_alarm_struct(int weekday, int hours_begin, int minutes_begin, int hours_end, int minutes_end)
{
	alarm_struct *str = malloc(sizeof(alarm_struct));
	str->weekday = weekday;
	str->hour_begin = hours_begin;
	str->minute_begin = minutes_begin;
	str->hour_end = hours_end;
	str->minute_end = minutes_end;
	str->isTriggered = 0;
	return str;
}

//Method to get alarms array
alarm_struct ** get_all_alarms()
{
	return alarms;
}

//get single alarm by weekday
alarm_struct *get_alarm(int weekday)
{
	if (weekday < 0 || weekday > 6)
	{
		printf("Value should be between 0 and 6!\n");
		return alarms[0];
	}
	else
	{
		return alarms[weekday];
	}
}

//Prints specified alarm
void print_alarm(alarm_struct *alarm)
{
	printf("Alarm: hour begin: %d, minute begin: %d, hour end: %d, minute end: %d, weekday:%d, triggerd: %d\n\n", alarm->hour_begin, alarm->minute_begin, alarm->hour_end, alarm->minute_end, alarm->weekday, alarm->isTriggered);
}

//Prints specified alarm. Index 0 for monday, 6 for sunday
void print_alarm_by_index(int alarm)
{
	printf("Alarm: hour begin: %d, minute begin: %d, hour end: %d, minute end: %d, weekday:%d, triggerd: %d\n\n", get_alarm(alarm)->hour_begin, get_alarm(alarm)->minute_begin, get_alarm(alarm)->hour_end, get_alarm(alarm)->minute_end, get_alarm(alarm)->weekday, get_alarm(alarm)->isTriggered);
}

//Prints all alarms
void print_all_alarms()
{
	printf("All alarms of the week:\n");
	int i = 0;
	for (; i < DAYS; i++)
	{
		print_alarm_by_index(i);
	}
}


void trigger_alarm(alarm_struct *alarm)
{
	menu_toggle_alarm_override();
	printf("Alarm Triggered!");
	if(retrieve_is_beep())
	{
		printf("\n---Internal Beep is used!---\n\n");
		current_alarm_status = dl_audio_ignite_the_beep();
	}
	else if (!retrieve_is_beep())
	{
		printf("\n---Stream is used!---\n\n");
		ll_inet_play_station(5);	//play radio 538
	}
	alarm->isTriggered = 1;
	//TODO: start snooze loop
}

//Retrieves the beep status. 
//When 0 is returned the stream is used. When 1 is returned the alarm is used.
int retrieve_is_beep()
{
	settings *setting = malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	int beep = setting->is_beep;
	free(setting);
	return beep;
}

void untrigger_alarm(alarm_struct *alarm)
{
	alarm->isTriggered = 0;
	current_alarm_status = 0;
}

//Method for testing re-arming an alarm,
//to make sure a detriggered alarm goes
//off the next day
void skip_a_day()
{
	tm *time = malloc(sizeof(tm));
	dl_rtc_get_clock(time);
	if(time->tm_wday == 6)
	{
		time->tm_wday =0;
	}
	else
	{
		time->tm_wday = time->tm_wday + 1;
	}
	
	time->tm_mday = time->tm_mday +1;
	time->tm_min = time->tm_min - 1;
	time->tm_sec = 50;
	dl_rtc_set_clock(time);
	printf("skipped a day!!");
}

//Trigger bit for all alarms is unset
void untrigger_all_alarms()
{
		unsigned int i = 0;
		for (; i < DAYS; i++)
		{
			untrigger_alarm(alarms[i]);
		}
		menu_toggle_alarm_override();
		dl_audio_stop_alarm_thread();
		//skip_a_day();
		//stopping the stream.
		ll_inet_stop_station();
}

void update_alarm(int tick_rate)
{
	tick_count += tick_rate;
	if (tick_count < ALARM_TICK_RATE)
	{
		return;
	}
	tm *time = malloc(sizeof(tm));
	dl_rtc_get_clock(time);
	int seconds = time->tm_sec;
	if(seconds <= 5)
	{
		unsigned i = 0;
		for(; i < DAYS; i++) 
		{
			if(time->tm_wday == get_alarm(i)->weekday) 
			{		
				//printf("\nWeekday same!\n");
				alarm_struct *str = get_alarm(i);

				if((time->tm_hour == str->hour_begin) && (!str->isTriggered)) 
				{
					// printf("\nHOUR same!\n");
					if(time->tm_min == str->minute_begin) 
					{
						printf("\nTrigger Alarm!\n");
						trigger_alarm(str);
						tick_count = 0;				
					}
				}
				else if(time->tm_hour == str-> hour_end) {
					if(time->tm_min == str-> minute_end) 
					{
						untrigger_all_alarms();
						tick_count = 0;			
					}
				}
			}
		}
	}
	else
	{
		tick_count = 0;
	}
	free(time);
}

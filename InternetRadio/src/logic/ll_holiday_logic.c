/*
 *  ll_holiday_logic.c
 *
 *  Created: 21-Mar-19 12:00:00 AM
 *  Author: Marck
 */

#include <time.h>
#include <stdio.h>
#include <string.h>

#include "driver/dl_rtc_driver.h"
#include "driver/dl_inet_driver.h"

#include "logic/ll_holiday_logic.h"
#include "logic/menu/IPR/util/ll_menu_IPR_util.h"

#define SECOND 1000


long tick_counter = 0;
char api_key[40] = "2aa8723c-942a-4f54-8df6-14c085492552";
char today[50];
int second_counter = 0;

/*
    The main logic function of this module

    Function to ask the api whether or not today is a holiday
    If today is not, the json doesn't contain a name
    This is caught by the checks in the .._retrieve_.. functions.

    param:  year    int     the year of the day you want to check
            month   int     the month of the day you want to check
            day     int     the date of the day you want to check
*/
void check_for_holidays(int year, int month, int day)
{
    char *json_holiday_on_error = "{\"status\":200,\"holidays\":[]}";
    char *json_test_holiday = "{\"status\":200,\"holidays\":[{\"name\":\"Koningsdag\",\"date\":\"2018-04-27\",\"observed\":\"2018-04-27\",\"public\":true}]}";
    
    char *holiday_name = malloc(sizeof(char) * 50);
    char *json_response;
    int connection_succes = 0;
    connection_succes = retrieve_holiday(&json_response);

    if(check_for_valid_json(json_test_holiday)) //<-- fill in the json that is recieved here
    {
        int succes;
        succes = retrieve_holiday_name(holiday_name, json_test_holiday);
        if(!succes)
        {
            holiday_name = "geen feestdag...";
        }
    }
    else
    {
        int succes;
        succes = retrieve_holiday_name(holiday_name, json_holiday_on_error);
        if(!succes)
        {
            holiday_name = "geen feestdag...";
        }
    }
    strcpy(today, holiday_name);
    free(holiday_name);
}

/*
    Function to check if the response is valid
    Status code 200 means everything okay
    Every other code means something went wrong

    param   json    const char *    the JSON response

    Returns 1 if status code is 200
    Returns 0 if status code is anything else
*/
int check_for_valid_json(char *json)
{
    char *status_code = malloc(sizeof(char) * 5);
    printf("[Feestdagen] - CHECK JSON VALIDNESS\n");
    int status;
    status = retrieve_status_code(status_code, json);
    if(200 == status)
    {
       printf("valid json! we can get the information!\n");
       return 1;
    }
    else
    {
        printf("no valid json\nuse a standard one\n");
        return 0;
    }
    free(status_code);
}

/*
    The update loop for this functionality
    This loop checks if its between 00:00 and 00:01
        if it is: Retrieve the data from the api
    if it is not: Do nothing

    param: tickrate     int     Used to count if a full second has passed
*/
void update(int tickrate)
{
    tick_counter += tickrate;
    
    //check if a second has passed
    //if not, return - else, up the seconds counter!
    if(tick_counter < SECOND)
    {
        return; 
    }
    second_counter++;
    if(59 == second_counter)    //check if the amount of seconds is 59 
    {                           //Then a minute has passed
        second_counter = 0;
    }
    else
    {
        return;
    }
    
    //get the current systems time from RTC chip
    tm *time = malloc(sizeof(tm));
    dl_rtc_get_clock(time);

    //check if its the first hour of the day
    if(0 == time->tm_hour)
    {
        //check if it's the first two minutes of the hour
        if(1 >= time->tm_min)
        {
            //it's a new day!
            //request the api for the info about today
            check_for_holidays(time->tm_year, (time->tm_mon + 1), time->tm_mday);
        }                                       //tm_mon in timestruct goes from
    }                                           //0-11, so +1 to get human value
}

/*
    Function to retrieve the holiday's name from the JSON
    If today is a holiday, the name is saved in the name
        and 1 is returned
    If today is not a holiday, the name won't contain data
        and 0 is returned

    params: name    char*   The char* in which the holiday name will be saved
                                                            (on succes)
            json    char*   The char* in which the JSON is stored which will
                            be cut apart
*/
int retrieve_holiday_name(char *name, char *json)
{
    char *output;
    int succes = 0;
    if(0 == getStringBetweenDelimiters(json, "name", "date", &output))
    {
        printf("Naam v1: %s\n", output);
        remove_garbage(output, name, 3);
        printf("Naam v2: %s\n", name);
        succes = 1;
    }
    else
    {   
       succes = 0;
    }
    free(output);

    return succes;
}

/*
    Function to retrieve the status code from a json
    
    param   status_string   char *  pointer to the string in which the code is stored
            json            char *  pointer to the json response
    
    returns the status code on succes
    returns 0 if the statuscode cannot be retrieved
*/
int retrieve_status_code(char *status_string, char *json)
{
    char *output;
    int *status_int = malloc(sizeof(int));
    if(0 == getStringBetweenDelimiters(json, "status", "holidays", &output))
    {
        printf("status v1: %s\n", output);
        remove_garbage(output, status_string, 2);
        printf("status v2: %s\n", status_string);
        util_convert_char_int(status_string, status_int);     
        printf("string \"%s\" conv to int: %d\n",*status_string, *status_int);
    }
    else
    {   
       status_int = 0;
    }
    free(output);
    return *status_int;
}

/*
    Function to retrieve the holiday of today
    returns either the name (in dutch), or "geen feestdag..."
*/
char *get_todays_holiday()
{
    return today;
}


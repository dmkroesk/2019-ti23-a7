/*
 * ll_clock_logic.h
 *
 * Created: 26-Feb-19 11:10:41 AM
 *  Author: Matthijs
 */
#ifndef LL_CLOCK_LOGIC_H_
#define LL_CLOCK_LOGIC_H_

#include <time.h>

typedef struct
{
	int weekday;		//weekday is 0 for sunday and 6 for saturday
	int hour_begin;		//Hour is 24 hour clock
	int minute_begin;	//0 to 60
	int hour_end;		//Hour is 24 hour clock
	int minute_end;		//0 to 60
	int isTriggered;	//0 for untriggered 1 for triggered
	int snooze;			//0 for off 1 for on
} alarm_struct;

//V2
alarm_struct *create_alarm_struct(int weekday, int hours_begin, int minutes_begin, int hours_end, int minutes_end);
alarm_struct **get_all_alarms(void);
alarm_struct *get_alarm(int weekday);
void set_alarm_for_day(alarm_struct *alarm, int weekday);
void set_alarm_for_week(void);
void untrigger_alarm(alarm_struct *alarm);

alarm_struct* get_triggered_alarm(void);

void set_alarm_final(int hour, int minute);
void print_all_alarms(void);
void init_alarms(void);
void update_alarm(int tick_rate);
void untrigger_all_alarms(void);
#endif /*  LL_CLOCK_LOGIC_H_ */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logic/menu/ll_menu_sleep_logic.h"
#include "driver/dl_rtc_driver.h"

#define STL_DEBUG
//struct used by stl to hold data for one day of the week.
struct sleep_time
{
    tm sleep_start_time;
    tm sleep_end_time;
    int is_past;
};

sleep_time **week = NULL;
int sleep_deficit = 0;

//boolean for detecting first launch
int stl_first_launch = 1;

int stl_get_first_launch()
{
    return stl_first_launch;
}

void stl_set_first_launch(int value)
{
    stl_first_launch = value;
}

//Starts a new week and allocates memory for it.
void stl_week_init()
{
    week = malloc(7 * sizeof(sleep_time *));

#ifdef STL_DEBUG
    printf("\n\n--TEST WEEK INIT--\n");
    printf("size of week: %d - address: %p\n", sizeof(week), &week);
#endif

    int i = 0;
    for (; i < 7; ++i)
    {
        week[i] = malloc(7 * sizeof(sleep_time));

#ifdef STL_DEBUG
        printf("size of sleep_time: %d - address: %p\n", sizeof(*week[i]), week[i]);
#endif
    }
}

//Sets the start time for sleep_time struct.
//Should be called when OK button in sleep_time menu is pressed.
void stl_set_start_time()
{
    tm *current_time = malloc(sizeof(tm));
    dl_rtc_get_clock(current_time);//TODO: check!
    sleep_time *sleep = stl_get_sleep_time_for_day(current_time->tm_wday);
    sleep->sleep_start_time.tm_wday = current_time->tm_wday;
    sleep->sleep_start_time.tm_hour = current_time->tm_hour;
    sleep->sleep_start_time.tm_min = current_time->tm_min;

#ifdef STL_DEBUG
    printf("\n\n--TEST SET START TIME\n");
    printf("set start time weekday: %d\n", sleep->sleep_start_time.tm_wday);
    printf("set start time hour: %d\n", sleep->sleep_start_time.tm_hour);
    printf("set start time minute: %d\n", sleep->sleep_start_time.tm_min);
#endif
}

//Sets the end time for the sleep_time.
//Should be called when alarm goes of for the first time.
void stl_set_end_time()
{
    tm *current_time = malloc(sizeof(tm));
    dl_rtc_get_clock(current_time);//TODO: check!

    //current_day is previous day because end time needs to be set.
    int current_day = (current_time->tm_wday == 0) ? (current_day = 6) : (current_day = current_time->tm_wday - 1);
    sleep_time *slept = stl_get_sleep_time_for_day(current_time->tm_wday);
    slept->sleep_end_time.tm_wday = current_day;
    slept->sleep_end_time.tm_hour = current_time->tm_hour;
    slept->sleep_end_time.tm_min = current_time->tm_min;
    slept->is_past = 1; //set passed flag to true

#ifdef STL_DEBUG
    printf("\n\n--TEST SET END TIME\n");
    printf("set start time weekday: %d\n", slept->sleep_end_time.tm_wday);
    printf("set start time hour: %d\n", slept->sleep_end_time.tm_hour);
    printf("set start time minute: %d\n", slept->sleep_end_time.tm_min);
#endif
}

//Returns an int with the amount of minutes slept the given day.
int stl_get_minutes_slept(stl_day day)
{
    sleep_time *st = stl_get_sleep_time_for_day(day);

    tm begin;
    begin.tm_hour = st->sleep_start_time.tm_hour;
    begin.tm_min = st->sleep_start_time.tm_min;

    tm end;
    end.tm_hour = st->sleep_end_time.tm_hour;
    end.tm_min = st->sleep_end_time.tm_min;

    //TODO: compare both tm structs to get the time difference.

    return 0;
}

//Returns the weekly sleep deficit.
int stl_calculate_weekly_sleep_deficit()
{
    sleep_deficit = 0; //reset sleep deficit so it can be calculated again.
    int i = 0;
    for (;i < 6; i++)
    {
        sleep_deficit += stl_get_minutes_slept(i); //calculate for each day and add together.
    }
    if (stl_get_sleep_time_for_day(SATURDAY)->is_past == 1)
    {
#ifdef STL_DEBUG
        printf("\n\n--WEEKLY SLEEP DEFICIT\n");
        printf("Sleep deficit written to database is \"%d\"\n", sleep_deficit);
        printf("Please implement write function...\n");
#endif
        //TODO: whole week is done. Write to database.
    }
    return sleep_deficit;
}

//Frees all memory previously used for sleep_time. Creates a new sleep_time and restarts the week.
int stl_start_new_week()
{
    //int week_sleep_deficit = sleep_deficit;   TODO: something with sleep_deficit
    int i = 0;
    for (; i < 7; ++i)
    {
        free(week[i]);
    }
    free(week);      //free all memory
    stl_week_init(); //and start the new week at sunday.
    return 0;
}

//Calculates the recommended alarm time from the current time.
tm *stl_calculate_recommended_alarm_time(tm *current_time)
{
    //sleep cycles take 90 minutes. It takes the average person up to 15 minutes to fall asleep.
    //8 hours of sleep contains 5 sleep cycles without the time to fall asleep taken into account.
    //A night of 8 hours has 495 minutes when 15 minutes are used to fall asleep.
    //495/90 = 5.5 sleep cycles. In the first 2-4 minutes of a sleep cycle you can be most easily waken up.
    //It would then be best to wake up at 495 - 40 = 455 minutes in.
    //This would mean that when you go to sleep at 11, you should wake up at 6:35  (455/60) = 7.5833 = 7 hours     0.5833 * 60 = 35 min
    //sources:https://www.tuck.com/stages/    https://www.sleepcycle.com/how-sleep-cycle-works/    https://thesleepdoctor.com/how-to-sleep-better/sleep-calculator/

    tm *wake_up_time = malloc(sizeof(tm));
    memset(wake_up_time, 0, sizeof(tm));
    int bedtime_hour = current_time->tm_hour;
    int bedtime_minutes = current_time->tm_min;

    int hours = 0;
    int minutes = (bedtime_minutes + STL_SLEEP_MINUTES) % 60;
    if (bedtime_minutes + STL_SLEEP_MINUTES > 60)
    {
        bedtime_hour++;
    }
    hours = (bedtime_hour + STL_SLEEP_HOURS) % 24;

    wake_up_time->tm_hour = hours;
    wake_up_time->tm_min = minutes;
    wake_up_time->tm_wday = current_time->tm_wday;
    return wake_up_time;
}

//Helper method to get sleep_time for given stl_day
sleep_time *stl_get_sleep_time_for_day(stl_day day)
{
    switch (day)
    {
    case SUNDAY:
        return week[0];
    case MONDAY:
        return week[1];
    case TUESDAY:
        return week[2];
    case WEDNESDAY:
        return week[3];
    case THURSDAY:
        return week[4];
    case FRIDAY:
        return week[5];
    case SATURDAY:
        return week[6];
    }
    return NULL;
}

//Returns if new week should start
int stl_is_new_week()
{
    tm *time = malloc(sizeof(time));
    dl_rtc_get_clock(time);
    int day = time->tm_wday;
    free(time);
    if(day == 0)
    {
        //day = sunday. New week has started
        return 1;
    }
    return 0;
}

//test method for sleep_time_for_day()
void stl_test_get_sleep_time_for_day()
{
    stl_get_sleep_time_for_day(SUNDAY)->sleep_start_time.tm_wday = SUNDAY;
    stl_get_sleep_time_for_day(MONDAY)->sleep_start_time.tm_wday = MONDAY;
    stl_get_sleep_time_for_day(TUESDAY)->sleep_start_time.tm_wday = TUESDAY;
    stl_get_sleep_time_for_day(WEDNESDAY)->sleep_start_time.tm_wday = WEDNESDAY;
    stl_get_sleep_time_for_day(THURSDAY)->sleep_start_time.tm_wday = THURSDAY;
    stl_get_sleep_time_for_day(FRIDAY)->sleep_start_time.tm_wday = FRIDAY;
    stl_get_sleep_time_for_day(SATURDAY)->sleep_start_time.tm_wday = SATURDAY;

    printf("\n\n--TEST GET SLEEP TIME FOR DAY--\n");
    printf("Test first day: %d - address: %p\n", stl_get_sleep_time_for_day(SUNDAY)->sleep_start_time.tm_wday, stl_get_sleep_time_for_day(SUNDAY));
    printf("Test first day: %d - address: %p\n", stl_get_sleep_time_for_day(MONDAY)->sleep_start_time.tm_wday, stl_get_sleep_time_for_day(MONDAY));
    printf("Test first day: %d - address: %p\n", stl_get_sleep_time_for_day(TUESDAY)->sleep_start_time.tm_wday, stl_get_sleep_time_for_day(TUESDAY));
    printf("Test first day: %d - address: %p\n", stl_get_sleep_time_for_day(WEDNESDAY)->sleep_start_time.tm_wday, stl_get_sleep_time_for_day(WEDNESDAY));
    printf("Test first day: %d - address: %p\n", stl_get_sleep_time_for_day(THURSDAY)->sleep_start_time.tm_wday, stl_get_sleep_time_for_day(THURSDAY));
    printf("Test first day: %d - address: %p\n", stl_get_sleep_time_for_day(FRIDAY)->sleep_start_time.tm_wday, stl_get_sleep_time_for_day(FRIDAY));
    printf("Test first day: %d - address: %p\n", stl_get_sleep_time_for_day(SATURDAY)->sleep_start_time.tm_wday, stl_get_sleep_time_for_day(SATURDAY));
}

//test method for calculate_recommended_alarm_time()
void stl_test_calculate_recommended_alarm_time()
{
    tm *test_time = malloc(sizeof(tm));
    test_time->tm_hour = 23;
    test_time->tm_min = 0;
    tm *result_time = stl_calculate_recommended_alarm_time(test_time);
    printf("\n\n--TEST CALCULATE RECOMMENDED ALARM TIME--\n");
    printf("Recommended alarm test hour: %d\n", result_time->tm_hour);
    printf("Recommended alarm test minute: %d\n", result_time->tm_min);
    free(test_time);
}

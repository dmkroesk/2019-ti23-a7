/*
 * ll_menu_settings.c
 *
 * Created: 27/02/2019 22:42:40
 *  Author: Patrick
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/ll_menu_settings_alarm.h"

void ll_menu_settings_alarm_draw(void);

menu_item ll_menu_settings_alarm_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_ALARM,
			.new_id =
				{
					MENU_SETTINGS,				// Esc
					MENU_SETTINGS_ALARM_EDITOR, // Ok
					MENU_SETTINGS_TIME,			// Up
					MENU_SETTINGS_TIMEZONE,		// Down
					MENU_NULL,					// Left
					MENU_NULL,					// Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_alarm_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_alarm_draw()
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	char buffer_hour[5];
	char buffer_minute[5];
	char *alarm_time = malloc(16 * sizeof(char));

	itoa(setting->alarm[0].hour_begin, buffer_hour, 10);
	itoa(setting->alarm[0].minute_begin, buffer_minute, 10);
	printf("MINUTE %s", buffer_minute);
	strcpy(alarm_time, buffer_hour);
	strcat(alarm_time, ":");
	strcat(alarm_time, buffer_minute);
	printf("STRING ALARM: %s\n", alarm_time);
	dl_display_multiline("Alarm:", alarm_time);

	free(alarm_time);
	free(setting);
}

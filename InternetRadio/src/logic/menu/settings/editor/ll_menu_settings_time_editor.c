/*
* ll_menu_settings_time_editor.c
*
* Created: 01/03/2019 09:25:27
*  Author: Patrick
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "driver/dl_display_driver.h"
#include "driver/dl_rtc_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/editor/ll_menu_settings_time_editor.h"

void ll_menu_settings_time_editor_draw(void);
void convert_time_disp(char *, tm *);
void move_arrow_left(void);
void move_arrow_right(void);

void decrement_num(void);
void increment_num(void);

void edit_hour(tm *hour, int change);
void edit_minute(tm *minute, int change);
void edit_second(tm *second, int change);
void save_time(void);
int arrow_pos = 0;
tm *time_struct;

menu_item ll_menu_settings_time_editor_init()
{
    menu_item item =
        {
            .id = MENU_SETTINGS_TIME_EDITOR,
            .new_id =
                {
                    MENU_SETTINGS_TIME, // Esc
                    MENU_NULL,          // Ok
                    MENU_NULL,          // Up
                    MENU_NULL,          // Down
                    MENU_NULL,          // Left
                    MENU_NULL,          // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_settings_time_editor_draw,
            .fp_on_key = {NULL, save_time, increment_num, decrement_num, move_arrow_left, move_arrow_right},
            .fp_on_entry = NULL,
            .fp_on_exit = NULL};

    time_struct = (tm *)malloc(sizeof(tm));
    dl_rtc_get_clock(time_struct);

    return item;
}

void save_time()
{
    dl_rtc_set_clock(time_struct);
    free(time_struct);
}
void ll_menu_settings_time_editor_draw()
{
    dl_display_clear();
    char *time_char = (char *)malloc(16 * sizeof(char));
    convert_time_disp(time_char, time_struct);
    dl_display_multiline("", time_char);
    dl_display_char_pos(1, 0, arrow_pos);
    free(time_char);
}

void increment_num()
{
    printf("STRUCT %i:%i:%i\n", time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);
    printf("INCREMENT - ARROW POS %i\n", arrow_pos);
    if (arrow_pos <= 1 && arrow_pos >= 0)
    {
        edit_hour(time_struct, 1);
        printf("INCREMENT - ARROW POS %i Changing hour by 1\n", arrow_pos);
        ll_menu_settings_time_editor_draw();
    }
    else
    {
        if (arrow_pos > 2 && arrow_pos <= 4)
        {
            edit_minute(time_struct, 1);
            printf("INCREMENT - ARROW POS %i Changing minute by 1\n", arrow_pos);
            ll_menu_settings_time_editor_draw();
        }
        else
        {
            if (arrow_pos >= 6 && arrow_pos <= 7)
            {
                edit_second(time_struct, 1);
                printf("INCREMENT - ARROW POS %i Changing second by 1\n", arrow_pos);
                ll_menu_settings_time_editor_draw();
            }
        }
    }
}

void decrement_num()
{
    if (arrow_pos <= 1 && arrow_pos >= 0)
    {
        edit_hour(time_struct, -1);
        ll_menu_settings_time_editor_draw();
    }
    else
    {
        if (arrow_pos >= 3 && arrow_pos <= 4)
        {
            edit_minute(time_struct, -1);
            ll_menu_settings_time_editor_draw();
        }
        else
        {
            if (arrow_pos >= 6 && arrow_pos <= 7)
            {
                edit_second(time_struct, -1);
                ll_menu_settings_time_editor_draw();
            }
        }
    }
}

void move_arrow_left()
{
    if (arrow_pos > 0)
    {
        arrow_pos = arrow_pos - 1;
        ll_menu_settings_time_editor_draw();
    }
    else
    {
        arrow_pos = 8;
        ll_menu_settings_time_editor_draw();
    }
}

void move_arrow_right()
{
    if (arrow_pos < 7)
    {
        arrow_pos = arrow_pos + 1;
        dl_display_clear();
        ll_menu_settings_time_editor_draw();
    }
    else
    {
        arrow_pos = 0;
        dl_display_clear();
        ll_menu_settings_time_editor_draw();
    }
}

void convert_time_disp(char *time_char, tm *time_struct)
{
    char hour_string[3];
    sprintf(hour_string, "%02d", time_struct->tm_hour);
    char minute_string[3];
    sprintf(minute_string, "%02d", time_struct->tm_min);
    char seconds_string[3];
    sprintf(seconds_string, "%02d", time_struct->tm_sec);

    strcpy(time_char, hour_string);
    strcat(time_char, ":");
    strcat(time_char, minute_string);
    strcat(time_char, ":");
    strcat(time_char, seconds_string);
}

void edit_hour(tm *hour, int change)
{
    if (time_struct->tm_hour < 23 && time_struct->tm_hour > 0)
    {
        time_struct->tm_hour = time_struct->tm_hour + change;
        return;
    }
    else
    {
        if (time_struct->tm_hour == 0 && change < 0)
        {
            time_struct->tm_hour = 23;
            return;
        }
        if (time_struct->tm_hour == 0 && change > 0)
        {
            time_struct->tm_hour = 1;
            return;
        }
        if (time_struct->tm_hour == 23 && change > 0)
        {
            time_struct->tm_hour = 0;
            return;
        }
        if (time_struct->tm_hour == 23 && change < 0)
        {
            time_struct->tm_hour = 22;
            return;
        }
    }
}

void edit_minute(tm *minute, int change)
{
    if (time_struct->tm_min < 59 && time_struct->tm_min > 0)
    {
        time_struct->tm_min = time_struct->tm_min + change;
        return;
    }
    else
    {
        if (time_struct->tm_min == 0 && change < 0)
        {
            time_struct->tm_min = 59;
            return;
        }
        if (time_struct->tm_min == 0 && change > 0)
        {
            time_struct->tm_min = 1;
            return;
        }
        if (time_struct->tm_min == 59 && change > 0)
        {
            time_struct->tm_min = 0;
            return;
        }
        if (time_struct->tm_min == 59 && change < 0)
        {
            time_struct->tm_min = 58;
            return;
        }
    }
}

void edit_second(tm *second, int change)
{
    if (time_struct->tm_sec < 59 && time_struct->tm_sec > 0)
    {
        time_struct->tm_sec = time_struct->tm_sec + change;
        return;
    }
    else
    {
        if (time_struct->tm_sec == 0 && change < 0)
        {
            time_struct->tm_sec = 59;
            return;
        }
        if (time_struct->tm_sec == 0 && change > 0)
        {
            time_struct->tm_sec = 1;
            return;
        }
        if (time_struct->tm_sec == 59 && change > 0)
        {
            time_struct->tm_sec = 0;
            return;
        }
        if (time_struct->tm_sec == 59 && change < 0)
        {
            time_struct->tm_sec = 58;
            return;
        }
    }
}

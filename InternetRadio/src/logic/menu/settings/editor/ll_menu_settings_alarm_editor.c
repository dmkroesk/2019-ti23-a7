/*
 * ll_menu_settings_alarm_editor.c
 *
 * Created: 01/03/2019 09:25:27
 *  Author: Patrick
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/editor/ll_menu_settings_alarm_editor.h"
#include "logic/ll_clock_logic.h"

void ll_menu_settings_alarm_editor_draw(void);
void convert_alarm_disp(char *);
void allocate_alarm(void);
void clean_alarm(void);

void decrement_alarm_num(void);
void increment_alarm_num(void);
void move_arrow_left_alarm(void);
void move_arrow_right_alarm(void);

void edit_hour_alarm(int change);
void edit_minute_alarm(int change);
void save_time_alarm(void);
int column_arrow_pos = 0;

settings *setting;
char *alarm_char;

menu_item ll_menu_settings_alarm_editor_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_ALARM_EDITOR,
			.new_id =
				{
					MENU_SETTINGS_ALARM, // Esc
					MENU_NULL,			 // Ok
					MENU_NULL,			 // Up
					MENU_NULL,			 // Down
					MENU_NULL,			 // Left
					MENU_NULL,			 // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_alarm_editor_draw,
			.fp_on_key = {NULL, save_time_alarm, increment_alarm_num, decrement_alarm_num, move_arrow_left_alarm, move_arrow_right_alarm},
			.fp_on_entry = allocate_alarm,
			.fp_on_exit = clean_alarm};
	return item;
}

void ll_menu_settings_alarm_editor_draw(void)
{
	dl_display_clear();

	convert_alarm_disp(alarm_char);
	dl_display_multiline("", alarm_char);
	dl_display_char_pos(1, 0, column_arrow_pos);
}

void allocate_alarm(void)
{
	setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	alarm_char = (char *)malloc(sizeof(char) * 16);
}

void clean_alarm(void)
{
	free(setting);
	free(alarm_char);
}

void convert_alarm_disp(char *alarm_char)
{
	char hour_string[3];
	sprintf(hour_string, "%02d", setting->alarm[0].hour_begin);
	char minute_string[3];
	sprintf(minute_string, "%02d", setting->alarm[0].minute_begin);

	strcpy(alarm_char, hour_string);
	strcat(alarm_char, ":");
	strcat(alarm_char, minute_string);
}

void decrement_alarm_num(void)
{
	if (column_arrow_pos <= 1 && column_arrow_pos >= 0)
	{
		edit_hour_alarm(-1);
		ll_menu_settings_alarm_editor_draw();
	}
	else
	{
		if (column_arrow_pos >= 3 && column_arrow_pos <= 4)
		{
			edit_minute_alarm(-1);
			ll_menu_settings_alarm_editor_draw();
		}
	}
}
void increment_alarm_num(void)
{
	if (column_arrow_pos <= 1 && column_arrow_pos >= 0)
	{
		edit_hour_alarm(1);
		ll_menu_settings_alarm_editor_draw();
	}
	else
	{
		if (column_arrow_pos >= 3 && column_arrow_pos <= 4)
		{
			edit_minute_alarm(1);
			ll_menu_settings_alarm_editor_draw();
		}
	}
}

void edit_hour_alarm(int change)
{
	if (setting->alarm[0].hour_begin < 23 && setting->alarm[0].hour_begin > 0)
	{
		setting->alarm[0].hour_begin = setting->alarm[0].hour_begin + change;
		return;
	}
	else
	{
		if (setting->alarm[0].hour_begin == 0 && change < 0)
		{
			setting->alarm[0].hour_begin = 23;
			return;
		}
		if (setting->alarm[0].hour_begin == 0 && change > 0)
		{
			setting->alarm[0].hour_begin = 1;
			return;
		}
		if (setting->alarm[0].hour_begin == 23 && change > 0)
		{
			setting->alarm[0].hour_begin = 0;
			return;
		}
		if (setting->alarm[0].hour_begin == 23 && change < 0)
		{
			setting->alarm[0].hour_begin = 22;
			return;
		}
	}
}
void edit_minute_alarm(int change)
{
	if (setting->alarm[0].minute_begin < 59 && setting->alarm[0].minute_begin > 0)
	{
		setting->alarm[0].minute_begin = setting->alarm[0].minute_begin + change;
		return;
	}
	else
	{
		if (setting->alarm[0].minute_begin == 0 && change < 0)
		{
			setting->alarm[0].minute_begin = 59;
			return;
		}
		if (setting->alarm[0].minute_begin == 0 && change > 0)
		{
			setting->alarm[0].minute_begin = 1;
			return;
		}
		if (setting->alarm[0].minute_begin == 59 && change > 0)
		{
			setting->alarm[0].minute_begin = 0;
			return;
		}
		if (setting->alarm[0].minute_begin == 59 && change < 0)
		{
			setting->alarm[0].minute_begin = 58;
			return;
		}
	}
}

void move_arrow_left_alarm()
{
	if (column_arrow_pos > 0)
	{
		column_arrow_pos = column_arrow_pos - 1;
		ll_menu_settings_alarm_editor_draw();
	}
	else
	{
		column_arrow_pos = 4;
		ll_menu_settings_alarm_editor_draw();
	}
}

void move_arrow_right_alarm()
{
	if (column_arrow_pos < 4)
	{
		column_arrow_pos = column_arrow_pos + 1;
		ll_menu_settings_alarm_editor_draw();
	}
	else
	{
		column_arrow_pos = 0;
		ll_menu_settings_alarm_editor_draw();
	}
}

void save_time_alarm(void)
{
	write_setting(setting, sizeof(settings));
	get_alarms_from_storage();
	set_alarm_final(setting->alarm[0].hour_begin, setting->alarm[0].minute_begin);
}

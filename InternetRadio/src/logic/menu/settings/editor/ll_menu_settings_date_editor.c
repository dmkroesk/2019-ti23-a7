/*
 * ll_menu_settings_alarm_editor.c
 *
 * Created: 01/03/2019 09:25:27
 *  Author: Patrick
 */
#include <string.h>
#include <stdio.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "driver/dl_rtc_driver.h"
#include "logic/menu/settings/editor/ll_menu_settings_date_editor.h"

void ll_menu_settings_date_editor_draw(void);
void edit_year(int change);
void edit_month(int change);
void edit_day(int change);

void move_arrow_date_left(void);
void move_arrow_date_right(void);
void decrement_num_date(void);
void increment_num_date(void);
void convert_date(char *date_char);
void save_time_date(void);

tm *time_struct;
int pos_arrow = 0;
menu_item ll_menu_settings_date_editor_init()
{
    menu_item item =
        {
            .id = MENU_SETTINGS_DATE_EDITOR,
            .new_id =
                {
                    MENU_SETTINGS_DATE, // Esc
                    MENU_NULL,          // Ok
                    MENU_NULL,          // Up
                    MENU_NULL,          // Down
                    MENU_NULL,          // Left
                    MENU_NULL,          // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_settings_date_editor_draw,
            .fp_on_key = {NULL, NULL, increment_num_date, decrement_num_date, move_arrow_date_left, move_arrow_date_right},
            .fp_on_entry = NULL,
            .fp_on_exit = save_time_date};

    time_struct = (tm *)malloc(sizeof(tm));
    dl_rtc_get_clock(time_struct);
    return item;
}

void save_time_date(void)
{
    printf("SAVING");
    dl_rtc_set_clock(time_struct);
    free(time_struct);
}

void ll_menu_settings_date_editor_draw(void)
{
    dl_display_clear();
    char *date_char = malloc(16 * sizeof(char));
    convert_date(date_char);
    dl_display_multiline("", date_char);
    dl_display_char_pos(1, 0, pos_arrow);
    free(date_char);
}

void convert_date(char *date_char)
{
    char year_string[5];
    sprintf(year_string, "%04d", (time_struct->tm_year + 1900));
    char month_string[3];
    sprintf(month_string, "%02d", (time_struct->tm_mon + 1));
    char day_string[3];
    sprintf(day_string, "%02d", time_struct->tm_mday);

    strcpy(date_char, day_string);
    strcat(date_char, "/");
    strcat(date_char, month_string);
    strcat(date_char, "/");
    strcat(date_char, year_string);
}

void move_arrow_date_left()
{
    if (pos_arrow > 0)
    {
        pos_arrow = pos_arrow - 1;
        ll_menu_settings_date_editor_draw();
    }
    else
    {
        pos_arrow = 9;
        ll_menu_settings_date_editor_draw();
    }
}

void move_arrow_date_right()
{
    if (pos_arrow < 9)
    {
        pos_arrow = pos_arrow + 1;
        dl_display_clear();
        ll_menu_settings_date_editor_draw();
    }
    else
    {
        pos_arrow = 0;
        dl_display_clear();
        ll_menu_settings_date_editor_draw();
    }
}

void edit_year(int change)
{
    int year = time_struct->tm_year + 1900;
    if (year < 2019 && year > 2000)
    {
        time_struct->tm_year = time_struct->tm_year + change;
        return;
    }
    else
    {
        if (year == 2000 && change < 0)
        {
            time_struct->tm_year = 119;
            return;
        }
        if (year == 2000 && change > 0)
        {
            time_struct->tm_year = 101;
            return;
        }
        if (year == 2019 && change > 0)
        {
            time_struct->tm_year = 100;
            return;
        }
        if (year == 2019 && change < 0)
        {
            time_struct->tm_year = 118;
            return;
        }
    }
}
void increment_num_date()
{
    if (pos_arrow <= 1 && pos_arrow >= 0)
    {
        edit_day(1);
        ll_menu_settings_date_editor_draw();
    }
    else
    {
        if (pos_arrow > 2 && pos_arrow <= 4)
        {
            edit_month(1);
            ll_menu_settings_date_editor_draw();
        }
        else
        {
            if (pos_arrow >= 6 && pos_arrow <= 9)
            {
                edit_year(1);
                ll_menu_settings_date_editor_draw();
            }
        }
    }
}

void decrement_num_date()
{
    if (pos_arrow <= 1 && pos_arrow >= 0)
    {
        edit_day(-1);
        ll_menu_settings_date_editor_draw();
    }
    else
    {
        if (pos_arrow >= 3 && pos_arrow <= 4)
        {
            edit_month(-1);
            ll_menu_settings_date_editor_draw();
        }
        else
        {
            if (pos_arrow >= 6 && pos_arrow <= 9)
            {
                edit_year(-1);
                ll_menu_settings_date_editor_draw();
            }
        }
    }
}

void edit_month(int change)
{
    if (time_struct->tm_mon < 11 && time_struct->tm_mon > 0)
    {
        time_struct->tm_mon = time_struct->tm_mon + change;
        return;
    }
    else
    {
        if (time_struct->tm_mon == 0 && change < 0)
        {
            time_struct->tm_mon = 11;
            return;
        }
        if (time_struct->tm_mon == 0 && change > 0)
        {
            time_struct->tm_mon = 1;
            return;
        }
        if (time_struct->tm_mon == 11 && change > 0)
        {
            time_struct->tm_mon = 0;
            return;
        }
        if (time_struct->tm_mon == 11 && change < 0)
        {
            time_struct->tm_mon = 10;
            return;
        }
    }
}

void edit_day(int change)
{
    if (time_struct->tm_mday < 31 && time_struct->tm_mday > 1)
    {
        time_struct->tm_mday = time_struct->tm_mday + change;
        return;
    }
    else
    {
        if (time_struct->tm_mday == 1 && change < 0)
        {
            time_struct->tm_mday = 31;
            return;
        }
        if (time_struct->tm_mday == 1 && change > 0)
        {
            time_struct->tm_mday = 2;
            return;
        }
        if (time_struct->tm_mday == 31 && change > 0)
        {
            time_struct->tm_mday = 1;
            return;
        }
        if (time_struct->tm_mday == 31 && change < 0)
        {
            time_struct->tm_mday = 30;
            return;
        }
    }
}

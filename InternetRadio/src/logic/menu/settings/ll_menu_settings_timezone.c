/*
 * ll_menu_settings.c
 *
 * Created: 27/02/2019 22:42:40
 *  Author: Patrick
 */

#include <stdlib.h>
#include <stdio.h>

#include "logic/menu/ll_data_menu.h"
#include "driver/dl_display_driver.h"
#include "driver/dl_rtc_driver.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/ll_menu_settings_timezone.h"
#include "driver/dl_rtc_driver.h"

void ll_menu_settings_timezone_draw(void);
static void decrement(void);
static void increment(void);
static void set_timezone(int timezone);


menu_item ll_menu_settings_timezone_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_TIMEZONE,
			.new_id =
				{
					MENU_SETTINGS,		  // Esc
					MENU_NULL,			  // Ok
					MENU_SETTINGS_ALARM,  // Up
					MENU_SETTINGS_VOLUME, // Down
					MENU_NULL,			  // Left
					MENU_NULL,			  // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_timezone_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, decrement, increment},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_timezone_draw()
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	char buffer[16];
	itoa(setting->time_zone, buffer, 10);
	dl_display_multiline("Tijd Zone:", buffer);
}

void decrement(void)
{
	printf("[Menu: Timezone] - Decrementing Timezone!\n");
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));

	if (setting->time_zone > -12)
	{
		setting->time_zone = setting->time_zone - 1;
	}
	else
	{
		setting->time_zone = 12;
	}
	set_timezone(setting->time_zone);
	write_setting(setting, sizeof(settings));
	ll_menu_settings_timezone_draw();
}

void increment(void)
{
	printf("[Menu: Timezone] - Incrementing Timezone!\n");
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	if (setting->time_zone < 12)
	{
		setting->time_zone = setting->time_zone + 1;
	}
	else
	{
		setting->time_zone = -12;
	}
	set_timezone(setting->time_zone);
	write_setting(setting, sizeof(settings));
	ll_menu_settings_timezone_draw();
}

void set_timezone(int timezone)
{
	if(timezone >= -12 && timezone <= 12)
	{
		dl_rtc_set_local_timezone(timezone);
	}
	
}


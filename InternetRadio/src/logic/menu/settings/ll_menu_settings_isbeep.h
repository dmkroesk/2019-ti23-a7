/*
 * ll_menu_settings_isbeep.h
 *
 * Created: 19-Mar-19 14:59:33
 *  Author: Daan
 */ 

#ifndef LL_MENU_SETTINGS_ISBEEP_H_
#define LL_MENU_SETTINGS_ISBEEP_H_

#include "logic/menu/ll_data_menu.h"

menu_item ll_menu_settings_isbeep_init(void);

#endif /* LL_MENU_SETTINGS_ISBEEP_H_ */

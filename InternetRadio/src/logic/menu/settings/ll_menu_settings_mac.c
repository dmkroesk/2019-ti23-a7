/*
 * ll_menu_settings.c
 *
 * Created: 27/02/2019 22:42:40
 *  Author: Patrick
 */

#include <stdlib.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/ll_menu_settings_mac.h"

void ll_menu_settings_mac_draw(void);

menu_item ll_menu_settings_mac_init(void)
{
	menu_item item =
		{
			.id = MENU_SETTINGS_MAC,
			.new_id =
				{
					MENU_SETTINGS,			   // Esc
					MENU_NULL,				   // Ok
					MENU_SETTINGS_ISBEEP, 	   // Up
					MENU_SETTINGS_DATE,		   // Down
					MENU_NULL,				   // Left
					MENU_NULL,				   // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_mac_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_mac_draw()
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	dl_display_multiline("MAC:", setting->mac_address);
}

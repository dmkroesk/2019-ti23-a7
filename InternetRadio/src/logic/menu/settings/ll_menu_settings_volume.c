/*
* ll_menu_settings.c
*
* Created: 27/02/2019 22:42:40
*  Author: Patrick
*/

#include <stdlib.h>
#include <stdio.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/ll_menu_settings_volume.h"

void ll_menu_settings_volume_draw(void);
static void decrement(void);
static void increment(void);

menu_item ll_menu_settings_volume_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_VOLUME,
			.new_id =
				{
					MENU_SETTINGS,			// Esc
					MENU_NULL,				// Ok
					MENU_SETTINGS_TIMEZONE, // Up
					MENU_SETTINGS_SUMMER_TIME,// Down
					MENU_NULL,				// Left
					MENU_NULL,				// Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_volume_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, decrement, increment},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_volume_draw()
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));

	char buffer[16];
	itoa(setting->volume, buffer, 10);
	dl_display_multiline("Volume:", buffer);
	free(setting);
}

void decrement(void)
{
	printf("[Menu: Volume] - Decrementing Volume!\n");
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	if (setting->volume > 0)
	{
		setting->volume = setting->volume - 1;
		write_setting(setting, sizeof(settings));
		ll_menu_settings_volume_draw();
	}
	free(setting);
}

void increment(void)
{
	printf("[Menu: Volume] - Incrementing Volume!\n");
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	if (setting->volume < 20)
	{
		setting->volume = setting->volume + 1;
		write_setting(setting, sizeof(settings));
		ll_menu_settings_volume_draw();
	}
	free(setting);
}

/*
 * ll_menu_settings.c
 *
 * Created: 27/02/2019 22:42:40
 *  Author: Patrick
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "driver/dl_display_driver.h"
#include "driver/dl_rtc_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/Il_menu_settings_date.h"

void ll_menu_settings_date_draw(void);

menu_item ll_menu_settings_date_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_DATE,
			.new_id =
				{
					MENU_SETTINGS,			   // Esc
					MENU_SETTINGS_DATE_EDITOR, // Ok
					MENU_SETTINGS_ISBEEP,	   // Up
					MENU_SETTINGS_TIME,		   // Down
					MENU_NULL,				   // Left
					MENU_NULL,				   // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_date_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_date_draw()
{
	tm *time_struct = (tm *)malloc(sizeof(tm));
	dl_rtc_get_clock(time_struct);
	char date[16];
	char year_string[5];
	sprintf(year_string, "%04d", (time_struct->tm_year + 1900));
	char month_string[3];
	sprintf(month_string, "%02d", (time_struct->tm_mon + 1));
	char day_string[3];
	sprintf(day_string, "%02d", time_struct->tm_mday);

	strcpy(date, day_string);
	strcat(date, "/");
	strcat(date, month_string);
	strcat(date, "/");
	strcat(date, year_string);

	dl_display_multiline("Datum:", date);
	free(time_struct);
}

/*
 * ll_menu_settings.c
 *
 * Created: 27/02/2019 22:42:40
 *  Author: Patrick
 */

#include <stdlib.h>
#include <stdio.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "driver/dl_storage_driver.h"
#include "logic/menu/settings/ll_menu_settings_summertime.h"

void ll_menu_settings_summertime_draw(void);
static void decrement(void);
static void increment(void);

menu_item ll_menu_settings_summertime_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_SUMMER_TIME,
			.new_id =
				{
					MENU_SETTINGS,		  // Esc
					MENU_NULL,			  // Ok
					MENU_SETTINGS_VOLUME, // Up
					MENU_SETTINGS_ISBEEP, // Down
					MENU_NULL,			  // Left
					MENU_NULL,			  // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_summertime_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, decrement, increment},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_summertime_draw()
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	char buffer[16];
	itoa(setting->summer_time, (char *)&buffer, 10);
	dl_display_multiline("Zomertijd:", buffer);
}

void decrement(void)
{
	printf("[Menu: Summer Time] - Decrementing Summer Time!\n");
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	if (setting->summer_time > 0)
	{
		setting->summer_time = 0;
		write_setting(setting, sizeof(settings));
		ll_menu_settings_summertime_draw();
	}
	else
	{
		setting->summer_time = 1;
		write_setting(setting, sizeof(settings));
		ll_menu_settings_summertime_draw();
	}
}

void increment(void)
{
	printf("[Menu: Summer Time] - Incrementing Summer Time!\n");
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	if (setting->summer_time < 1)
	{
		setting->summer_time = 1;
		write_setting(setting, sizeof(settings));
		ll_menu_settings_summertime_draw();
	}
	else
	{
		setting->summer_time = 0;
		write_setting(setting, sizeof(settings));
		ll_menu_settings_summertime_draw();
	}
}

/*
 * ll_menu_settings_isbeep.c
 *
 * Created: 19-Mar-19 14:59:15
 *  Author: Daan
 */ 

#include <stdlib.h>
#include <stdio.h>

#include "driver/dl_display_driver.h"
#include "driver/dl_storage_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/settings/ll_menu_settings_isbeep.h"

void ll_menu_settings_isbeep_draw(void);
void toggle(void);

menu_item ll_menu_settings_isbeep_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_ISBEEP,
			.new_id =
				{
					MENU_SETTINGS,			   // Esc
					MENU_NULL, 				   // Ok
					MENU_SETTINGS_SUMMER_TIME, // Up
					MENU_SETTINGS_DATE,		   // Down
					MENU_NULL,				   // Left
					MENU_NULL,				   // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_isbeep_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, toggle, toggle},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_isbeep_draw()
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));

    char *value;
    if (setting->is_beep == 1)
        value = "Aan";
    else
        value = "Uit";
    
	dl_display_multiline("Piepen bij alarm:", value);
    free(setting);
}

void toggle()
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
    int value = setting->is_beep;
	value++;
    if (value >= 2)
        value = 0;
    
    setting->is_beep = value;
    write_setting(setting, sizeof(settings));
    ll_menu_settings_isbeep_draw();
    free(setting);
}

/*
 * ll_menu_settings_snooze.h
 *
 * Created: 28/02/2019 12:28:41
 *  Author: Patrick
 */

#ifndef LL_MENU_SETTINGS_SNOOZE_H_
#define LL_MENU_SETTINGS_SNOOZE_H_

menu_item ll_menu_settings_snooze_init(void);

#endif /* LL_MENU_SETTINGS_SNOOZE_H_ */

/*
 * ll_menu_settings.c
 *
 * Created: 27/02/2019 22:42:40
 *  Author: Patrick
 */

#include <string.h>
#include <stdio.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "logic/menu/settings/Il_menu_settings_time.h"
#include "driver/dl_rtc_driver.h"

void ll_menu_settings_time_draw(void);
void convert_time(char *time_char, tm *time_struct);

int edit_mode;
int column = 0;

menu_item
ll_menu_settings_time_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS_TIME,
			.new_id =
				{
					MENU_SETTINGS,			   // Esc
					MENU_SETTINGS_TIME_EDITOR, // Ok
					MENU_SETTINGS_DATE,		   // Up
					MENU_SETTINGS_ALARM,	   // Down
					MENU_NULL,				   // Left
					MENU_NULL,				   // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_time_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void convert_time(char *time_char, tm *time_struct)
{
	char hour_string[3];
	sprintf(hour_string, "%02d", time_struct->tm_hour);
	char minute_string[3];
	sprintf(minute_string, "%02d", time_struct->tm_min);
	char seconds_string[3];
	sprintf(seconds_string, "%02d", time_struct->tm_sec);

	strcpy(time_char, hour_string);
	strcat(time_char, ":");
	strcat(time_char, minute_string);
	strcat(time_char, ":");
	strcat(time_char, seconds_string);
}

void ll_menu_settings_time_draw()
{
	char *time_char = malloc(16 * sizeof(char));
	tm *time_struct = (tm *)malloc(sizeof(tm));
	dl_rtc_get_clock(time_struct);
	convert_time(time_char, time_struct);
	dl_display_multiline("Tijd:", time_char);
	free(time_char);
}

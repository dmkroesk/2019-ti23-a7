/*
 * ll_menu_settings_timezone.h
 *
 * Created: 28/02/2019 12:06:15
 *  Author: Patrick
 */

#ifndef LL_MENU_SETTINGS_TIMEZONE_H_
#define LL_MENU_SETTINGS_TIMEZONE_H_

menu_item ll_menu_settings_timezone_init(void);

#endif /* LL_MENU_SETTINGS_TIMEZONE_H_ */

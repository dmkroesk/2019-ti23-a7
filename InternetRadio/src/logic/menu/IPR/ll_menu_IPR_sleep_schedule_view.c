/*
 * ll_menu_IPR_sleep_schedule_view.c
 *
 * Created: 16/03/2019 17:49:25
 *  Author: Matthijs
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "driver/dl_display_driver.h"
#include "driver/dl_rtc_driver.h"
#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_sleep_logic.h"
#include "logic/menu/IPR/ll_menu_IPR_sleep_schedule_view.h"

void ll_menu_ipr_sleep_schedule_view_draw(void);
void sleep_schedule_open(void);
void sleep_schedule_close(void);
void sleep_schedule_recommended_alarm_time(void);
void sleep_schedule_start_time(void);

menu_item ll_menu_ipr_sleep_schedule_view_init()
{
    menu_item item =
        {
            .id = MENU_IPR_SLEEP_SCHEDULE_VIEW,
            .new_id =
                {
                    MENU_IPR_SLEEP_SCHEDULE, // Esc
                    MENU_NULL,               // Ok
                    MENU_NULL,               // Up
                    MENU_NULL,               // Down
                    MENU_NULL,              // Left
                    MENU_NULL               // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_ipr_sleep_schedule_view_draw,
            .fp_on_key = {NULL, NULL, sleep_schedule_recommended_alarm_time, sleep_schedule_start_time, NULL, NULL},
            .fp_on_entry = sleep_schedule_open,
            .fp_on_exit = NULL};
    return item;
}

void ll_menu_ipr_sleep_schedule_view_draw()
{
    char buffer[30];
    int sleep_deficit = stl_calculate_weekly_sleep_deficit();
    sprintf(buffer, "%d", sleep_deficit);
    dl_display_multiline("Slaaptekort", buffer);
}

void sleep_schedule_open()
{
    printf("NEW WEEK: %d\n", stl_is_new_week());
    if(stl_get_first_launch())
    {
        stl_set_first_launch(0);
        stl_week_init();
    }
}

void sleep_schedule_recommended_alarm_time()
{
    char buffer_hour[10];
    char buffer_min[10];

    tm *current_time;
    printf("Made current_time\n");
    dl_rtc_get_clock(current_time);
    printf("Get clock\n");
    tm *alarm = stl_calculate_recommended_alarm_time(current_time);
    printf("calculate alarm time\n");
    int alarm_time_hour = alarm->tm_hour;
    int alarm_time_minute = alarm->tm_min;

    sprintf(buffer_hour, "%d :", alarm_time_hour);
    sprintf(buffer_min, "%d uur", alarm_time_minute);

    char string[10];
    strcat(string, buffer_hour);
    strcat(string, buffer_min);

    dl_display_multiline("Aanbevolen alarm", string);
}

void sleep_schedule_start_time()
{
    dl_display_multiline("Slaap", "vastleggen...");
    stl_set_start_time();
}

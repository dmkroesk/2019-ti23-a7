/*
 * ll_menu_IPR_dino_game.c
 *
 * Created: 16/03/2019 17:46:58
 *  Author: Daan
 */

#include <stdlib.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/IPR/ll_menu_IPR_dino_game.h"

void ll_menu_ipr_dino_game_draw(void);

menu_item ll_menu_ipr_dino_game_init()
{
    menu_item item =
        {
            .id = MENU_IPR_DINO_GAME,
            .new_id =
                {
                    MENU_IPR,            // Esc
                    MENU_IPR_DINO_GAME_PLAY,// Ok
                    MENU_NULL,           // Up
                    MENU_NULL,           // Down
                    MENU_IPR_RSS_FEED,   // Left
                    MENU_IPR_OPENWEATHER // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_ipr_dino_game_draw,
            .fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
            .fp_on_entry = NULL,
            .fp_on_exit = NULL};
    return item;
}

void ll_menu_ipr_dino_game_draw()
{
    dl_display_multiline("Dino Spel", "Door Daan");
    dl_display_char_pos(2, 0, 12);
}

/*
 * ll_menu_IPR_RSS_feed.c
 *
 * Created: 16/03/2019 17:50:35
 *  Author: Gerben
 */

#include <stdlib.h>

#include <string.h>
#include "logic/menu/ll_data_menu.h"
#include "driver/dl_display_driver.h"
#include "driver/dl_inet_driver.h"
#include "logic/ll_inet_logic.h"
#include "logic/menu/IPR/ll_menu_IPR_RSS_feed.h";
#include "logic/menu/ll_menu_ipr.h";
#include "driver/dl_rss_driver.h"


char* output_text;
char* display_string [16];

char *p_json;

int index = 0;

void ll_menu_ipr_rss_draw(void);
void on_entry(void);


menu_item ll_menu_ipr_rss_init()
{
    menu_item item =
        {
            .id = MENU_IPR_RSS_FEED,
            .new_id =
                {
                    MENU_IPR,                 // Esc
                    MENU_NULL,                // Ok
                    MENU_NULL,                // Up
                    MENU_NULL,                // Down
                    MENU_IPR_CELEBRATION_DAY, // Left
                    MENU_IPR_DINO_GAME        // Right
                },
            .refresh_rate = 500,
            .fp_draw = ll_menu_ipr_rss_draw,
            .fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
            .fp_on_entry = on_entry,
            .fp_on_exit = NULL};
    return item;
}

void on_entry() {
    test_rss();
}

void ll_menu_ipr_rss_draw()
{
    //printf("Update!\n");
    if(get_text() != 0) {
        //printf("gettext!\r\n");
        output_text = get_text();
    }
    //printf("Text: %s\r\n", output_text);

    int length = strlen(output_text); //90
    //printf("DRAW: Index: %i\n", index);
    if(index >= (length - 15)) {
        printf("RESET INDEX Start by front\n");
        index = 0;
    }
    index++;

    //printf("Index: %i\n", index);
    //printf("Output Text: %s\n", output_text);
    //dl_display_multiline("RSS Feed", output_text);

    loop_chars_on_display();
}

void loop_chars_on_display() {
    strncpy(display_string, output_text + index, 15);
    //printf("Display_String: %s\n", display_string);
    display_string[16] = '\0';
    //printf("Display_String FINAL: %s", display_string);
    dl_display_clear();
    dl_display_multiline("RSS Feed", display_string);
}

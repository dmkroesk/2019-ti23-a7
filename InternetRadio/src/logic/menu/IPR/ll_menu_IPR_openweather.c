/*
 * ll_menu_IPR_openweather.c
 *
 * Created: 16/03/2019 17:48:52
 *  Author: Patrick
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/timer.h>
#include <sys/thread.h>

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_ipr.h"
#include "driver/dl_display_driver.h"
#include "logic/ll_inet_logic.h"
#include "driver/dl_inet_driver.h"
#include "logic/menu/IPR/ll_menu_IPR_openweather.h"
#include "logic/menu/IPR/util/ll_menu_IPR_util.h"
#include "driver/dl_inet_driver.h"

void ll_menu_ipr_openweather_entry(void);
void ll_menu_ipr_openweather_draw(void);

menu_item ll_menu_ipr_openweather_init()
{
    menu_item item =
        {
            .id = MENU_IPR_OPENWEATHER,
            .new_id =
                {
                    MENU_IPR,               // Esc
                    MENU_NULL,              // Ok
                    MENU_NULL,              // Up
                    MENU_NULL,              // Down
                    MENU_IPR_DINO_GAME,     // Left
                    MENU_IPR_SLEEP_SCHEDULE // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_ipr_openweather_entry,
            .fp_on_key = {NULL, ll_menu_ipr_openweather_draw, NULL, NULL, NULL, NULL},
            .fp_on_entry = NULL,
            .fp_on_exit = NULL};
    return item;
}

/**
 * This is the first screen you see on entry, Will clear the display and then display "Openweather" on the first line
 * And on the second it writes "Door Patrick"
 */
void ll_menu_ipr_openweather_entry(void)
{
    printf("[Openweather] - Entered Openweather Screen\n");
    dl_display_clear();
    dl_display_multiline("Actuele weer", "Door Patrick");
}

/**
 * The main screen to draw
 * Shows three weather items of the JSON
 * The Temperature (in Celsius), Wind speed (km/h), and Amount of rain (mm)
 */
void ll_menu_ipr_openweather_draw(void)
{
    // Logging information
    printf("[Openweather] - Drawing Openweather\n");
    // Clearing the display after the entry screen
    dl_display_clear();

    // Weather struct to easily organize and increase further information fast, less hardcoding
    weather_struct weather;

    // Json to fill when there is internet from API Call
    char *json_pointer;
    // Hardcoded JSONs
    char *json_weather = "{\"liveweer\":[{\"plaats\":\"Breda\",\"temp\":\"11.7\",\"gtemp\":\"9.3\",\"samenv\":\"Zwaarbewolkt\",\"lv\":\"77\",\"windr\":\"NW\",\"windms\":\"4\",\"winds\":\"3\",\"windk\":\"7.8\",\"windkmh\":\"14.4\",\"luchtd\":\"1034.8\",\"ldmmhg\":\"776\",\"dauwp\":\"7\",\"zicht\":\"11\",\"verw\":\"Wolkenveldenendroog\",\"sup\":\"06:38\",\"sunder\":\"18:56\",\"image\":\"bewolkt\",\"d0weer\":\"halfbewolkt\",\"d0tmax\":\"17\",\"d0tmin\":\"3\",\"d0windk\":\"2\",\"d0windknp\":\"4\",\"d0windms\":\"2\",\"d0windkmh\":\"7\",\"d0windr\":\"ZO\",\"d0neerslag\":\"0\",\"d0zon\":\"40\",\"d1weer\":\"halfbewolkt\",\"d1tmax\":\"17\",\"d1tmin\":\"5\",\"d1windk\":\"2\",\"d1windknp\":\"6\",\"d1windms\":\"3\",\"d1windkmh\":\"11\",\"d1windr\":\"Z\",\"d1neerslag\":\"10\",\"d1zon\":\"50\",\"d2weer\":\"bewolkt\",\"d2tmax\":\"11\",\"d2tmin\":\"3\",\"d2windk\":\"2\",\"d2windknp\":\"6\",\"d2windms\":\"3\",\"d2windkmh\":\"11\",\"d2windr\":\"N\",\"d2neerslag\":\"20\",\"d2zon\":\"20\",\"alarm\":\"0\"}]}";

    //Display to let the user know that it neededs to connect to the internet first.
    dl_display_multiline("Verbinden inet", "Even wachten aub!");

    /**
     *  Checking if there is internet, ll_inet_connected will return 0 if there is no internet and then will go the else which will use the Hardcoded JSON.
     *  If it will return 1 it will if it can make an API Call if yes then it will retrieve JSON. if not it will use the hardcoded JSON.
     * 
     */
    if (ll_inet_connected() == TRUE)
    {
        // Api call function
        retrieve_weather(&json_pointer);
        if (strlen(json_pointer) > EMPTY)
        {
            printf("RETRIEVE ITEMS FROM JSON\n");
            printf("JSON FROM INET %s\n", json_pointer);
            get_data(weather, json_pointer, 4);
        }
        else
        {
            // Let the users know that there is an error
            dl_display_multiline("Error: fout ophalen api", "Gebruik oude data!");
            NutSleep(3000);
            // Use the hardcoded json and retrieve the information from the hardcoded json.
            get_data(weather, json_weather, 3);
        }
    }
    else
    {
        dl_display_multiline("Error: Geen inet", "Gebruik oude data!");
        NutSleep(3000);

        get_data(weather, json_weather, 3);
    }

    //Adding the abbreviation into my struct
    strcpy(weather.celsius.abbreviation, "C");
    strcpy(weather.wind_speed.abbreviation, "Ws");
    strcpy(weather.rain.abbreviation, "Ne");
    dl_display_string_pos(weather.celsius.abbreviation, 0, CELSIUS_POS);
    dl_display_string_pos(weather.wind_speed.abbreviation, 0, WIND_SPEED_POS);
    dl_display_string_pos(weather.rain.abbreviation, 0, RAIN_POS);
}

/**
 * Retrieve the temperature from the JSON. It will look for a value between "temp" and "gtemp" and then remove the garbage value's infront of it
 * @param temperature this will fill the char array and be the final output
 * @param json this is the raw json
 */
void retrieve_temp(char *temperature, char *json, int change)
{
    char *output;
    if (getStringBetweenDelimiters(json, "temp", "gtemp", &output) == 0)
    {
        printf("[Openweather] - Found value between temp and gtemp\n");
        remove_garbage(output, temperature, change);
    }

    free(output);
}

/**
 * Retrieve the wind speed from the JSON. It will look for a value between "windkmh" and "luchtd" and then remove the garbage value's infront of it
 * @param wind_speed this will fill the char array and be the final output
 * @param json this is the raw json
 */
void retrieve_wind_speed(char *wind_speed, char *json, int change)
{
    char *output;
    if (getStringBetweenDelimiters(json, "windkmh", "luchtd", &output) == 0)
    {
        printf("[Openweather] - Found value between windkmh and luchtd\n");
        remove_garbage(output, wind_speed, change);
    }

    free(output);
}

/**
 * Retrieve the wind speed from the JSON. It will look for a value between "d0neerslag" and "d0zon" and then remove the garbage value's infront of it
 * @param wind_type this will fill the char array and be the final output
 * @param json this is the raw json
 */
void retrieve_rain(char *wind_type, char *json, int change)
{
    char *output;
    if (getStringBetweenDelimiters(json, "d0neerslag", "d0zon", &output) == 0)
    {
        printf("[Openweather] - Found value between d0neerslag and d0zon\n");
        remove_garbage(output, wind_type, change);
    }

    free(output);
}

/**
 * get data takes the json and fill the struct.
 * @param weather is the weather struct used to hold all information, like abbreviations and values
 * @param json_weather The hardcoded or API call string json 
 */
void get_data(weather_struct weather, char *json_weather, int change)
{
    dl_display_clear();
    retrieve_temp(weather.celsius.value, json_weather, change);
    retrieve_wind_speed(weather.wind_speed.value, json_weather, change);
    retrieve_rain(weather.rain.value, json_weather, change);

    if (strlen(weather.celsius.value) > EMPTY)
    {
        dl_display_string_pos(weather.celsius.value, 1, CELSIUS_POS);
    }
    else
    {
        dl_display_string_pos("Er", 1, CELSIUS_POS);
    }

    if (strlen(weather.wind_speed.value) > EMPTY)
    {
        dl_display_string_pos(weather.wind_speed.value, 1, WIND_SPEED_POS);
    }
    else
    {
        dl_display_string_pos("Er", 1, WIND_SPEED_POS);
    }

    if (strlen(weather.rain.value) > EMPTY)
    {
        dl_display_string_pos(weather.rain.value, 1, RAIN_POS);
    }
    else
    {
        dl_display_string_pos("Er", 1, RAIN_POS);
    }
}

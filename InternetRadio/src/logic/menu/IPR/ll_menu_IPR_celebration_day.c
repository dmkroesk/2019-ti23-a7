/*
 * ll_menu_IPR_celebration_day.c
 *
 * Created: 16/03/2019 17:50:11
 *  Author: Marck
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_ipr.h"
#include "driver/dl_display_driver.h"
#include "logic/ll_inet_logic.h"
#include "logic/menu/IPR/ll_menu_IPR_celebration_day.h"
#include "logic/ll_holiday_logic.h"
#include "logic/menu/IPR/util/ll_menu_IPR_util.h"

void ll_menu_ipr_celebration_draw(void);

menu_item ll_menu_ipr_celebration_init()
{
    menu_item item =
        {
            .id = MENU_IPR_CELEBRATION_DAY,
            .new_id =
                {
                    MENU_IPR,                // Esc
                    MENU_NULL,               // Ok
                    MENU_NULL,               // Up
                    MENU_NULL,               // Down
                    MENU_IPR_SLEEP_SCHEDULE, // Left
                    MENU_IPR_RSS_FEED        // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_ipr_celebration_entry,
            .fp_on_key = {NULL, ll_menu_ipr_celebration_draw, NULL, NULL, NULL, NULL},
            .fp_on_entry = NULL,
            .fp_on_exit = NULL};
    return item;
}

void ll_menu_ipr_celebration_entry()
{
    printf("[Feestdagen]  -  Entered holiday screen!\n");
    dl_display_clear();
    dl_display_multiline("Feestdagen", "Door Marck");
}

void ll_menu_ipr_celebration_draw()
{
    printf("[Feestdagen] - feestdagen_draw()\n");
    dl_display_clear();
    /*
    for now a raw JSON string. After HTTP service is working properly,
    this will be changed to requesting a JSON from the API
    */
    char *json_holiday_pointer = malloc(sizeof(char) * 50);
    check_for_holidays(0,0,0);

    json_holiday_pointer = get_todays_holiday();
    dl_display_multiline("Het is vandaag", json_holiday_pointer);
}



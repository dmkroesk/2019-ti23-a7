/*
 * ll_menu_IPR_sleep_schedule.c
 *
 * Created: 16/03/2019 17:49:25
 *  Author: Matthijs
 */
#include <stdlib.h>

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_ipr.h"
#include "driver/dl_display_driver.h"
#include "logic/ll_inet_logic.h"
#include "logic/menu/IPR/ll_menu_IPR_sleep_schedule.h"

void ll_menu_ipr_sleep_schedule_draw(void);

menu_item ll_menu_ipr_sleep_schedule_init()
{
    menu_item item =
        {
            .id = MENU_IPR_SLEEP_SCHEDULE,
            .new_id =
                {
                    MENU_IPR,                // Esc
                    MENU_IPR_SLEEP_SCHEDULE_VIEW,                  // Ok
                    MENU_NULL,    // Up
                    MENU_NULL,               // Down
                    MENU_IPR_OPENWEATHER,    // Left
                    MENU_IPR_CELEBRATION_DAY // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_ipr_sleep_schedule_draw,
            .fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
            .fp_on_entry = NULL,
            .fp_on_exit = NULL};
    return item;
}

void ll_menu_ipr_sleep_schedule_draw()
{
    dl_display_multiline("Slaap Schema", "Door Matthijs");
}

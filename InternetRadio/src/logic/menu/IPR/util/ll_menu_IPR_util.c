/*
 * ll_menu_IPR_openweather_util.c
 *
 * Created: 16/03/2019 17:48:52
 *  Author: Patrick
 */

#include "logic/menu/IPR/util/ll_menu_IPR_util.h"
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

void util_convert_int_char(char *buffer, int *num)
{
    snprintf(buffer, 4, "%s", *num);
}

void util_convert_char_int(char *buffer, int *num)
{
    *num = atoi(buffer);
}

int getStringBetweenDelimiters(const char *json, const char *leftDelimiter, const char *rightDelimiter, char **output)
{
    // find the left delimiter and use it as the beginning of the substring
    const char *beginning = strstr(json, leftDelimiter);
    if (beginning == NULL)
        return 1; // left delimiter not found

    // find the right delimiter`
    const char *end = strstr(json, rightDelimiter);
    if (end == NULL)
        return 2; // right delimiter not found
    // offset the beginning by the length of the left delimiter, so beginning points _after_ the left delimiter
    beginning += strlen(leftDelimiter);

    // get the length of the substring
    ptrdiff_t segmentLength = end - beginning;
    // allocate memory and copy the substring there
    *output = malloc(segmentLength + 1);
    if (*output == 0)
    {
        printf("BROKEN seg length%i", segmentLength);
    }
    strncpy(*output, beginning, segmentLength);
    (*output)[segmentLength] = 0;
    return 0; // success!
}

void remove_garbage(char *value, char *output, int amount)
{
    value[strlen(value) - amount] = 0;
    strcpy(output, &value[amount]);
}

/*
 * ll_menu_IPR_openweather.h
 *
 * Created: 16/03/2019 17:49:00
 *  Author: Patrick
 */

#ifndef LL_MENU_IPR_UTIL_H_
#define LL_MENU_IPR_UTIL_H_

#define SIZE(x) sizeof(x) / sizeof(x[0])

void util_convert_char_int(char *, int *);
void util_convert_int_char(char *, int *);
void remove_garbage(char *, char *, int);
int getStringBetweenDelimiters(const char *, const char *, const char *, char **);
#endif /* LL_MENU_IPR_UTIL_H_ */


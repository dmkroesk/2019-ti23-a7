/*
 * ll_menu_IPR_celebration_day.h
 *
 * Created: 16/03/2019 17:50:21
 *  Author: Marck
 */

#ifndef LL_MENU_IPR_CELEBRATION_DAY_H_
#define LL_MENU_IPR_CELEBRATION_DAY_H_

void ll_menu_ipr_celebration_entry(void);
menu_item ll_menu_ipr_celebration_init(void);
int retrieve_holiday_name(char *name, char *json);

#endif /* LL_MENU_IPR_CELEBRATION_DAY_H_ */

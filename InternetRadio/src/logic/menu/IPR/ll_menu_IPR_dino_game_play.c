/*
 * ll_menu_IPR_dino_game_play.c
 *
 * Created: 16/03/2019 17:46:58
 *  Author: Daan
 */

#include <stdlib.h>
#include <stdio.h>

#include "logic/menu/ll_data_menu.h"
#include "logic/ll_dino_logic.h"
#include "logic/menu/IPR/ll_menu_IPR_dino_game_play.h"

#define REFRESH_RATE 50

void dino_draw(void);
void dino_init(void);
void dino_start(void);
void dino_end(void);

menu_item ll_menu_ipr_dino_game_play_init()
{
    menu_item item =
        {
            .id = MENU_IPR_DINO_GAME_PLAY,
            .new_id =
                {
                    MENU_IPR_DINO_GAME,  // Esc
                    MENU_NULL,           // Ok
                    MENU_NULL,           // Up
                    MENU_NULL,           // Down
                    MENU_NULL,           // Left
                    MENU_NULL            // Right
                },
            .refresh_rate = REFRESH_RATE,
            .fp_draw = dino_draw,
            .fp_on_key = {NULL, dino_start, NULL, NULL, NULL, NULL}, // Most keys get updated in the logic, because they need to act different
            .fp_on_entry = dino_init,
            .fp_on_exit = dino_end};
    return item;
}

void dino_draw()
{
    ll_dino_update(); // The game gets drawn in the update function
}

void dino_init()
{
    ll_dino_init(REFRESH_RATE);
}

void dino_start()
{
    ll_dino_start();
}

void dino_end()
{
    ll_dino_end();
}

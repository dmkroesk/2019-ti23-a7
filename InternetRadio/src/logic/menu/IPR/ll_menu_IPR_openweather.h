/*
 * ll_menu_IPR_openweather.h
 *
 * Created: 16/03/2019 17:49:00
 *  Author: Patrick
 */

#ifndef LL_MENU_IPR_OPENWEATHER_H_
#define LL_MENU_IPR_OPENWEATHER_H_
#include "logic/menu/ll_data_menu.h"

#define TRUE 1
#define FALSE 0
#define EMPTY 0

#define CELSIUS_POS 0
#define WIND_SPEED_POS 6
#define RAIN_POS 13

typedef struct
{
    char abbreviation[3];
    char value[4];
} node;

typedef struct
{
    node celsius;
    node wind_speed;
    node rain;
} weather_struct;

menu_item ll_menu_ipr_openweather_init(void);
void retrieve_temp(char *, char *, int);
void retrieve_wind_speed(char *, char *, int);
void retrieve_rain(char *, char *, int);
void get_data(weather_struct, char *, int);

#endif /* LL_MENU_IPR_OPENWEATHER_H_ */

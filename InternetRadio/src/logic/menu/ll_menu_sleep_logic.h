#ifndef SLEEP_TIME_LIBRARY_H_
#define SLEEP_TIME_LIBRARY_H_

#include <time.h>

//amount of hours to sleep when wanting to sleep 8 hours.
#define STL_SLEEP_HOURS 7

//amount of minutes to sleep when wanting to sleep 8 hours.
#define STL_SLEEP_MINUTES 35

//days of the week used by stl
enum stl_day{ SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY };
typedef enum stl_day stl_day;

//struct used by stl to hold data for one day of the week.
typedef struct sleep_time sleep_time;

extern int stl_get_first_launch(void);

extern void stl_set_first_launch(int);

//Initialization method for stl.
extern void stl_week_init(void);

//Sets the sleep start time for a day.
extern void stl_set_start_time(void);

//Sets the sleep end time for a day.
extern void stl_set_end_time(void);

//Returns if new week should start
extern int stl_is_new_week(void);

//Returns the total amount of minutes slept for a day.
extern int stl_get_minutes_slept(stl_day day);

//Returns the weekly sleep deficit.
extern int stl_calculate_weekly_sleep_deficit(void);

//Starts a new week. Old week is deleted and stl_week_init is called.
extern int stl_start_new_week(void);

//Calculates the recommended time to set your alarm for when taking 8 hours of sleep.
extern tm * stl_calculate_recommended_alarm_time(tm *current_time);

//Calculates the total sleep time for a given day.
extern sleep_time * stl_get_sleep_time_for_day(stl_day day);

static void stl_test_get_sleep_time_for_day(void);
static void stl_test_calculate_recommended_alarm_time(void);

#endif //SLEEP_TIME_LIBRARY_H_

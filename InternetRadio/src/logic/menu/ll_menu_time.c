/*
 * ll_menu_time.c
 *
 * Created: 26-2-2019 16:41:00
 *  Author: Daan
 */

#include <stdlib.h>

#include "driver/dl_display_driver.h"
#include "driver/dl_rtc_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_time.h"
#include "logic/ll_inet_logic.h"

menu_item ll_menu_time_init()
{
	menu_item item =
		{
			.id = MENU_TIME,
			.new_id =
				{
					MENU_NULL,	 // Esc
					MENU_NULL,	 // Ok
					MENU_NULL,	 // Up
					MENU_NULL,	 // Down
					MENU_IPR, 	 // Left
					MENU_ALARM	 // Right
				},
			.refresh_rate = 1000,
			.fp_draw = ll_menu_time_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_time_draw()
{
	//printf("ll_menu_time_draw()\n");
	char time[20];
	char date[20];
	dl_rtc_get_current_time(time, date);
	dl_display_multiline(time, date);
	int result = ll_inet_connected();
	if (result == 1)
	{
		dl_display_custom_char(0);
	}
}

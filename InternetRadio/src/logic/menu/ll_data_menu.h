/*
 * ll_menu_data.h
 *
 * Created: 27/02/2019 21:19:32
 *  Author: Daan
 */

#ifndef LL_DATA_MENU_H_
#define LL_DATA_MENU_H_

#define MAX_MENU_KEY 6
#define MAX_MENU_ITEMS 100

/* Define indices for the keys in the array of new IDs */
#define MENU_KEY_ESC 0
#define MENU_KEY_OK 1
#define MENU_KEY_UP 2
#define MENU_KEY_DOWN 3
#define MENU_KEY_LEFT 4
#define MENU_KEY_RIGHT 5

/* Define the IDs for all menu items */
#define MENU_NULL -1

// Start menu
#define MENU_START 0

// Time menu
#define MENU_TIME 1

// Alarm menu
#define MENU_ALARM 2

// Settings Menu
#define MENU_SETTINGS 3
#define MENU_SETTINGS_DATE 10
#define MENU_SETTINGS_TIME 11
#define MENU_SETTINGS_ALARM 12
#define MENU_SETTINGS_TIMEZONE 13
#define MENU_SETTINGS_VOLUME 14
#define MENU_SETTINGS_SNOOZE 15
#define MENU_SETTINGS_SUMMER_TIME 16
#define MENU_SETTINGS_MAC 17
#define MENU_SETTINGS_ISBEEP 18
#define MENU_SETTINGS_ALARM_EDITOR 19
#define MENU_SETTINGS_TIME_EDITOR 20
#define MENU_SETTINGS_DATE_EDITOR 21

// IPR menu
#define MENU_IPR 50
#define MENU_IPR_DINO_GAME 30		// DAAN
#define MENU_IPR_DINO_GAME_PLAY 31	// DAAN
#define MENU_IPR_OPENWEATHER 32		// PATRICK
#define MENU_IPR_SLEEP_SCHEDULE 33  // MATTHIJS
#define MENU_IPR_SLEEP_SCHEDULE_VIEW 34  // MATTHIJS
#define MENU_IPR_CELEBRATION_DAY 35 // MARCK
#define MENU_IPR_RSS_FEED 36		// GERBEN

/* Define struct to hold data for a single menu item */
typedef struct
{
	char id;							   /* ID for this item */
	char new_id[MAX_MENU_KEY];			   /* IDs to jump to on keypress */
	int refresh_rate;				   	   /* Milliseconds between screen refreshes */
	void (*fp_draw)(void);				   /* Text for this item */
	void (*fp_on_key[MAX_MENU_KEY])(void); /* Function pointer for each key */
	void (*fp_on_entry)(void);			   /* Function called on entry */
	void (*fp_on_exit)(void);			   /* Function called on exit */
} menu_item;

#endif /* LL_DATA_MENU_H_ */

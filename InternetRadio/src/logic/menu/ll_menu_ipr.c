/*
 * ll_menu_ipr.c
 *
 * Created: 16/03/2019 16:34:09
 *  Author: Patrick
 */
#include <stdlib.h>

#include "driver/dl_display_driver.h"

#include "logic/ll_inet_logic.h"
#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_ipr.h"

void ll_menu_ipr_draw(void);

menu_item ll_menu_ipr_init()
{
    menu_item item =
        {
            .id = MENU_IPR,
            .new_id =
                {
                    MENU_TIME,       // Esc
                    MENU_IPR_DINO_GAME,// Ok
                    MENU_NULL,       // Up
                    MENU_NULL,       // Down
                    MENU_SETTINGS,   // Left
                    MENU_TIME        // Right
                },
            .refresh_rate = 0,
            .fp_draw = ll_menu_ipr_draw,
            .fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
            .fp_on_entry = NULL,
            .fp_on_exit = NULL};
    return item;
}

void ll_menu_ipr_draw()
{
    dl_display_multiline("IPR", "");
    int result = ll_inet_connected();
	if (result == 1)
		dl_display_custom_char(0);
}

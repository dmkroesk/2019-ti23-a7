
/*
 * ll_menu_time.h
 *
 * Created: 26-2-2019 16:40:43
 *  Author: Daan
 */

#ifndef LL_MENU_TIME_H_
#define LL_MENU_TIME_H_

#include "logic/menu/ll_data_menu.h"

menu_item ll_menu_time_init(void);
void ll_menu_time_draw(void);

#endif /* LL_MENU_TIME_H_ */

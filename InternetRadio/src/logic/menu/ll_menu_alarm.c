/*
 * ll_menu_alarm.c
 *
 * Created: 27/02/2019 22:47:10
 *  Author: Daan
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_alarm.h"
#include "logic/ll_inet_logic.h"
#include "driver/dl_storage_driver.h"
#include "driver/dl_rtc_driver.h"

void ll_menu_alarm_draw(void);
void convert_alarm_time(char *alarm);
void clean(void);
void allocate(void);

settings *setting;
tm *time_s;
char *alarm;

char *days[7] = {"Zo", "Ma", "Di", "Wo", "Do", "Vr", "Za"};

menu_item
ll_menu_alarm_init()
{
	menu_item item =
		{
			.id = MENU_ALARM,
			.new_id =
				{
					MENU_TIME,	// Esc
					MENU_NULL,	// Ok
					MENU_NULL,	// Up
					MENU_NULL,	// Down
					MENU_TIME,	// Left
					MENU_SETTINGS // Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_alarm_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = allocate,
			.fp_on_exit = clean};

	return item;
}

void ll_menu_alarm_draw()
{
	convert_alarm_time(alarm);
	dl_display_multiline("Alarm tijd", alarm);
	dl_display_custom_char(0);
	int day = time_s->tm_wday;
	char *asd = days[day];
	dl_display_string_pos(asd, 1, 14);

	int result = ll_inet_connected();
	if (result == 1)
	{
		dl_display_custom_char(0);
	}
}

void convert_alarm_time(char *alarm)
{
	char buffer_hour[5];
	char buffer_minute[5];
	itoa(setting->alarm[0].hour_begin, buffer_hour, 10);
	itoa(setting->alarm[0].minute_begin, buffer_minute, 10);
	printf("MINUTE %s", buffer_minute);
	strcpy(alarm, buffer_hour);
	strcat(alarm, ":");
	strcat(alarm, buffer_minute);
	printf("STRING ALARM: %s\n", alarm);
}

void allocate(void)
{
	time_s = (tm *)malloc(sizeof(tm));
	dl_rtc_get_clock(time_s);
	setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	alarm = (char *)malloc(sizeof(char) * 16);
}

void clean(void)
{
	free(setting);
	free(alarm);
}

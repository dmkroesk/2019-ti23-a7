/*
 * ll_menu_settings.c
 *
 * Created: 27/02/2019 22:42:40
 *  Author: Daan
 */

#include <stdlib.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_settings.h"
#include "logic/ll_inet_logic.h"

void ll_menu_settings_draw(void);

menu_item ll_menu_settings_init()
{
	menu_item item =
		{
			.id = MENU_SETTINGS,
			.new_id =
				{
					MENU_TIME,			// Esc
					MENU_SETTINGS_DATE, // Ok
					MENU_NULL,			// Up
					MENU_NULL,			// Down
					MENU_ALARM,			// Left
					MENU_IPR			// Right
				},
			.refresh_rate = 0,
			.fp_draw = ll_menu_settings_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_settings_draw()
{
	dl_display_multiline("Instellingen", "");
	dl_display_custom_char(0);
	int result = ll_inet_connected();
	if (result == 1)
	{
		dl_display_custom_char(0);
	}
}

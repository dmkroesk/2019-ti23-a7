/*
 * ll_menu_start.c
 *
 * Created: 21-3-2019 9:41:00
 *  Author: Daan
 */

#include <stdlib.h>

#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_start.h"

void ll_menu_start_draw(void);

menu_item ll_menu_start_init()
{
	menu_item item =
		{
			.id = MENU_START,
			.new_id =
				{
					MENU_NULL,	 // Esc
					MENU_NULL,	 // Ok
					MENU_NULL,	 // Up
					MENU_NULL,	 // Down
					MENU_NULL, 	 // Left
					MENU_NULL	 // Right
				},
			.refresh_rate = 100,
			.fp_draw = ll_menu_start_draw,
			.fp_on_key = {NULL, NULL, NULL, NULL, NULL, NULL},
			.fp_on_entry = NULL,
			.fp_on_exit = NULL};
	return item;
}

void ll_menu_start_draw()
{
	dl_display_multiline("    Starting", "    Radio...");
}

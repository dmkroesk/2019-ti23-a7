/*
 * ll_inet_logic.c
 *
 * Created: 26-Feb-19 11:11:37 AM
 *  Author: Daan
 */

#include <stdio.h>
#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "driver/dl_inet_driver.h"
#include "logic/ll_inet_logic.h"

#define TRUE 1
#define FALSE 0

int inited = FALSE;
int connected = FALSE;
int starting_stream = FALSE;
int streaming = FALSE;
int current_station = 0;

THREAD(inet_init_thread, arg);
THREAD(inet_play_thread, arg);

void ll_inet_init()
{
	if (inited == TRUE)
		return;
	inited = TRUE;
	printf("initing inet logic...\n");
	NutThreadCreate("inet_init", inet_init_thread, NULL, 512);
}

THREAD(inet_init_thread, arg)
{
	// If the connection failed
	if (dl_inet_init() != 0)
	{
		;
	}
	else
	{
		// If the connection has been made succesfully
		if (dl_inet_get_time())
			connected = CONNECTED;
	}
	NutThreadExit();
}

int ll_inet_connected(void)
{
	// TODO: Check connection again...
	return connected;
}

void ll_inet_play_station(int station_number)
{
	// Do nothing if not connected
	if (connected == NOT_CONNECTED)
		return;

	printf("is connected\n");

	// If station is already playing or connecting do nothing
	if (current_station == station_number)
		if (streaming == TRUE || starting_stream == TRUE)
			return;

	printf("is not playing the station\n");

	// if other stream is connecting, stop the started stream
	if (starting_stream == TRUE)
	{
		printf("stopping connecting\n");
		starting_stream = FALSE;
	}

	if (streaming == TRUE)
	{
		streaming = FALSE;
		dl_inet_stream_stop();
	}
	starting_stream = TRUE;
	current_station = station_number;
	NutThreadCreate("inet_init", inet_play_thread, NULL, 512);
}

THREAD(inet_play_thread, arg)
{
	if (dl_inet_stream_connect(current_station, &starting_stream) == 0)
	{
		starting_stream = FALSE;
		streaming = TRUE;
		dl_inet_stream_play();
	}
	NutThreadExit();
}

void ll_inet_stop_station()
{
	if (connected == NOT_CONNECTED || current_station == 0)
		return;

	printf("stopping station alt button\n");
	starting_stream = FALSE;
	current_station = 0;
	streaming = FALSE;
	dl_inet_stream_stop();
}

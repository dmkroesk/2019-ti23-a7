/*
 * ll_dino_logic.h
 *
 * Created: 16/03/2019 17:46:58
 *  Author: Daan
 */

#ifndef DL_DINO_LOGIC_H_
#define DL_DINO_LOGIC_H_

void ll_dino_init(int);
void ll_dino_start(void);
void ll_dino_update(void);
void ll_dino_end(void);

#endif /* DL_DINO_LOGIC_H_ */

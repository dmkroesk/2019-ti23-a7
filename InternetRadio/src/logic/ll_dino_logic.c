/*
 * ll_dino_logic.c
 *
 * Created: 16/03/2019 17:46:58
 * Author: Daan
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "hardware/hl_keyboard.h"
#include "driver/dl_storage_driver.h"
#include "driver/dl_display_driver.h"
#include "logic/ll_dino_logic.h"

// Types of movement states
#define SPAWNED     0
#define STOPPED     1
#define RUNNING     2
#define JUMPING     3
#define DUCKING     4

// Types of blocks (also corresponds with the index of the custom chars)
#define NOTHING     0
#define DINO_1      2
#define DINO_2      3
#define DINO_DUCK   4
#define CACTUS      5
#define BIRD_1      6
#define BIRD_2      7

// Other constants
#define DINO_BLOCK          4
#define BASE_SPEED          5
#define MAX_JUMP_LENGTH     15
#define SPEED_MULTIPLIER    40

// Game logic variables that hold values for the game
char state;
int tick_rate;
char ticks_jumping;

char game_speed;
int ms_since_scroll;
char blocks_since_object;
char dino_animation_frame;
char bird_animation_frame;

int score;
int highscore;

// The game field holds the objects for each collum (can be a bird, a cactus or nothing)
char game_field[COLS];

// Private game logic functions
void ll_dino_start_game(void);
void ll_dino_check_buttons(void);
bool ll_dino_check_collision(int);
bool ll_dino_check_scroll(void);
void ll_dino_scroll_game_field(void);
void ll_dino_update_score(void);
void ll_dino_draw_game(void);
void ll_dino_draw_game_screen(void);
void ll_dino_draw_splash_screen(void);
void ll_dino_draw_game_over_screen(void);
void ll_dino_end_game(void);

// Initializes the game, and displays the splash screen
void ll_dino_init(int tick)
{
    printf("Dino Game init...\n");
    tick_rate = tick;
    state = SPAWNED;
    ll_dino_draw_game();
}

// If the game is not already running, it runs the game
void ll_dino_start()
{
    if (state == SPAWNED || state == STOPPED)
        ll_dino_start_game();
}

// Update function for the main game logic (needs to get called every game tick)
void ll_dino_update()
{
    // Check if the game is running, if not, return the function
    if (state == SPAWNED || state == STOPPED)
        return;

    // Check buttons and update state
    ll_dino_check_buttons();

    // Check collision (for this block)
    if (ll_dino_check_collision(0))
    {
        ll_dino_end_game();
        return;
    }

    // Check if the game field needs to be scrolled
    if (ll_dino_check_scroll())
    {
        // Check collision (for the next block)
        if (ll_dino_check_collision(1))
        {
            ll_dino_end_game();
            return;
        }

        // Scroll game field (and generate new objects)
        ll_dino_scroll_game_field();

        // Update score (and change speed if neccesary)
        ll_dino_update_score();
    }

    // Draw game
    ll_dino_draw_game();
}

// Calls the end game function
void ll_dino_end()
{
    ll_dino_end_game();
}

// Starts the game and initializes all variables
void ll_dino_start_game()
{
    // Initialize all of the game variables (mostly set them to zero)
    printf("Game has started\n");
    state = RUNNING;
    ticks_jumping = 0;
    game_speed = BASE_SPEED;
    ms_since_scroll = 0;
    blocks_since_object = 0;
    dino_animation_frame = 0;
    bird_animation_frame = 0;
    score = 0;
    highscore = 0;

    // Init game field to all nothing
    int i;
    for (i = 0; i < COLS; i++)
        game_field[i] = NOTHING;
}

// Ends the game and saves the score into flash memory if it's a highscore
void ll_dino_end_game()
{
    printf("Game over with score %d\n", score);
    state = STOPPED;

    // If it's a highscore, save it in flash storage
    settings *setting = (settings *)malloc(sizeof(settings));
    read_setting(setting, sizeof(settings));

    highscore = setting->dino_highscore;
    if (highscore < score)
    {
        highscore = score;
        setting->dino_highscore = score;
        write_setting(setting, sizeof(settings));
    }
    free(setting);

    ll_dino_draw_game();
}

// Looks at which button is pressed, and updates the game logic accordingly
void ll_dino_check_buttons()
{
    // Get the currently pressed key and check for the game inputs
    int current_key = KbGetKey();

    // Check all the other game buttons
    switch (current_key)
    {
    case KEY_ESC: // Stop game button
        ll_dino_end_game();
        break;

    case KEY_UP: // Jump button
        if (ticks_jumping != -1 && ticks_jumping < MAX_JUMP_LENGTH)
        {
            state = JUMPING;
            ticks_jumping++;
        }
        else
        {
            state = RUNNING;
            ticks_jumping = -1;
        }
        break;

    case KEY_DOWN: // Duck button
        state = DUCKING;
        ticks_jumping = 0;
        break;

    default: // Nothing is pressed
        state = RUNNING;
        ticks_jumping = 0;
        break;
    }
}

// Checks if the Dino has collided with the current block or the next, depending on the shift
bool ll_dino_check_collision(int shift)
{
    // If the next block is a cactus, check if the player is jumping
    if (game_field[DINO_BLOCK + shift] == CACTUS && state != JUMPING)
        return true;

    // If the next block is a bird, check if the player is ducking
    if (game_field[DINO_BLOCK + shift] == BIRD_1 && state != DUCKING)
        return true;

    // If the dino survived all of that, it can continue
    return false;
}

// Looks if the field needs to scroll this tick, depending on the game speed
bool ll_dino_check_scroll()
{
    // Check if the game field needs to scroll based on the time since last tick and the game speed
    if (ms_since_scroll > 1000 / game_speed)
    {
        ms_since_scroll = 0;
        return true;
    }
    ms_since_scroll += tick_rate;
    return false;
}

// Scrolls the game field along and generates new objects randomly
void ll_dino_scroll_game_field()
{
    char next_block = NOTHING; // Sets the next block to nothing

    // If the previous object was spawned of the previous 2 blocks, don't spawn another
    if (blocks_since_object > 1)
    {
        char random = rand() % 100; // gets a random number from 0 to 9 to decide on the next obstacle
        if (random <= 10)           // 10% chance to spawn a cactus
            next_block = CACTUS;
        if (random >= 95)           // 5% chance to spawn a bird
            next_block = BIRD_1;

        if (next_block != NOTHING)  // If an object was spawned, set the amount of blocks to 0
            blocks_since_object = 0;
        else                        // Else, increment the counter
            blocks_since_object++;
    }
    else
    {
        blocks_since_object++;
    }

    int i; // Loop through the field array, and shift all the objects to the left and add the next block
    for (i = 0; i < COLS - 1; i++)
        game_field[i] = game_field[i + 1];
    game_field[COLS - 1] = next_block;
}

// Updates the score and sets the increases the game speed if necessary
void ll_dino_update_score()
{
    score++;

    // If max game speed has been reached, return
    if (game_speed >= 1000 / tick_rate)
        return;

    // Else, check if the speed needs to go up
    if (score >= (game_speed - BASE_SPEED) * SPEED_MULTIPLIER)
        game_speed++;
}

// Draws the Dino Game, depending on the state, this will call different draw functions
void ll_dino_draw_game()
{
    if (state == SPAWNED)
        ll_dino_draw_splash_screen();
    else if (state == STOPPED)
        ll_dino_draw_game_over_screen();
    else
        ll_dino_draw_game_screen();
}

// Draws the game, the player, all of the objects and the score to the screen
void ll_dino_draw_game_screen()
{
    dl_display_clear();

    int i;
    for (i = 0; i < COLS; i++)
    {
        if (game_field[i] == CACTUS)
            dl_display_char_pos(CACTUS, 1, i);
        else if (game_field[i] == BIRD_1 && bird_animation_frame < 4)
            dl_display_char_pos(BIRD_1, 0, i);
        else if (game_field[i] == BIRD_1)
            dl_display_char_pos(BIRD_2, 0, i);
    }

    // Toggle which frame of the animation the dino will display
    dino_animation_frame = (dino_animation_frame + 1) % 4;
    bird_animation_frame = (bird_animation_frame + 1) % 8;

    // Display Dino
    if (state == JUMPING)
        dl_display_char_pos(DINO_1, 0, DINO_BLOCK);
    else if (state == DUCKING)
        dl_display_char_pos(DINO_DUCK, 1, DINO_BLOCK);
    else if (dino_animation_frame < 2)
        dl_display_char_pos(DINO_1, 1, DINO_BLOCK);
    else
        dl_display_char_pos(DINO_2, 1, DINO_BLOCK);

    // Draw score
    char score_string[DINO_BLOCK + 1];
    if (score < (int)pow(10, DINO_BLOCK))
        sprintf(score_string, "%d", score);
    else
        strcpy(score_string, "MAX");
    dl_display_string_pos(score_string, 0, 0);
}

// Draws the splash screen of the game
void ll_dino_draw_splash_screen()
{
    dl_display_multiline("   Dino Spel", "  Druk op OK");
    dl_display_char_pos(DINO_1, 0, 0);
    dl_display_char_pos(CACTUS, 0, 15);
}

// Draws the game over screen with the reached score, and the highscore
void ll_dino_draw_game_over_screen()
{
    char bottom[COLS + 1];
    char hs[10];
    sprintf(bottom, "Score: %d", score);
    sprintf(hs, "%d", highscore);
    dl_display_multiline("Spel over   HS:", bottom);
    dl_display_string_pos(hs, 1, 12);
}

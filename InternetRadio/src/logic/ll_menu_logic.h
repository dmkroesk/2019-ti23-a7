/*
 * ll_menu_logic.h
 *
 * Created: 21-Feb-19 14:31:26
 *  Author: Daan
 */

#ifndef LL_MENU_LOGIC_H_
#define LL_MENU_LOGIC_H_

extern void ll_menu_init(void);
extern void ll_menu_update(int);
extern int menu_toggle_alarm_override(void);
#endif /* LL_MENU_LOGIC_H_ */

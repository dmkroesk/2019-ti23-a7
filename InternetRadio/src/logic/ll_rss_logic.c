/*
 * ll_rss_logic_.c
 *
 * Created: 21-Mar-19 9:17:21 AM
 *  Author: Gerben
 */ 

//----------------------
#include <sys/socket.h>
#include "logic/ll_rss_logic.h"
#include <string.h>

TCPSOCKET *sock;
FILE *stream;
char buff[1024];
char *ret;
char temp[6] = "temp";

char buff[1024];

// FILE *connect_to_rss(const char *ip, const int port, const char *route)
// {
// 	char      *data;
// 	//TCPSOCKET *socket;
// 	FILE      *streamrss;

// 	socket = NutTcpCreateSocket();
// 	printf("Connecting to ip : %s\nwith Port : %d\nand Route: %s\n",ip,port,route);

//     NutTcpConnect(socket, inet_addr("167.114.118.40"), 80);

// 	if (NutTcpConnect(socket, inet_addr(ip), 80))
// 	{
// 		printf("Error: >> NutTcpConnect() <<");
// 		return NULL;
// 	}
// 	else
// 	{
// 		printf("SUCCES: >> NutTcpConnect() <<");

// 		streamrss = _fdopen((int)socket, "r+b");

// 		fprintf(streamrss, "GET %s HTTP/1.0\r\n", route); //prepare request
// 		fprintf(streamrss, "Host: %s\r\n", ip);
// 		fprintf(streamrss, "User-Agent: Ethernut\r\n");
// 		fprintf(streamrss, "Connection: close\r\n\r\n");
// 		fflush(streamrss);

// 		// Server sends response, catch it
// 		data = (char *)malloc(512 * sizeof(char));

// 		char* temp;
// 		 while((temp = fgets(data, 512, streamrss)) != NULL) //keep going till stream is empty
// 		 {
// 		 		printf("%s", temp); //Print response contents
// 		 };

// 		printf("\n %s \n", "Done downloading");
//         printf("%d", streamrss);
// 		return streamrss;
// 	}
// }

int ethernet_getRequest(char* ip, int port, char* route, char* host) {  
    int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;

    /* Connecting to api */
    sock = NutTcpCreateSocket();
    NutTcpSetSockOpt(sock, 0x02, &segmentation, sizeof(segmentation));

	NutTcpSetSockOpt(sock, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));	

	NutTcpSetSockOpt(sock, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));
    
   	NutTcpConnect(sock, inet_addr(ip), port);

	stream = _fdopen((int) sock, "r+b");

	fprintf(stream, "GET %s HTTP/1.0\r\n", route);
    fprintf(stream, "Host: %s\r\n", host);
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: /\r\n");
    fprintf(stream, "Connection: close\r\n\r\n");
    fflush(stream);


    while (fgets(buff, sizeof(buff), stream)) {
        puts(buff);
    }

    ret = strstr(buff,temp);

    fclose(stream);
    NutTcpCloseSocket(sock);
    return 1;
}

/*
 * ll_inet_logic.h
 *
 * Created: 26-Feb-19 11:11:47 AM
 *  Author: Daan
 */
#ifndef ll_INET_LOGIC_H_
#define ll_INET_LOGIC_H_

#define CONNECTED 1
#define NOT_CONNECTED 0

void ll_inet_init(void);
int ll_inet_connected(void);

void ll_inet_play_station(int);
void ll_inet_stop_station(void);

#endif /*  ll_INET_LOGIC_H_ */

/*
* ll_menu_logic.c
*
* Created: 21-Feb-19 14:31:14
*  Author: Daan
*/

#include <stdio.h>
#include <stdlib.h>

#include "hardware/hl_keyboard.h"

#include "driver/dl_keyboard_driver.h"
#include "driver/dl_display_driver.h"

#include "logic/menu/ll_data_menu.h"
#include "logic/ll_menu_logic.h"
#include "logic/ll_inet_logic.h"
#include "logic/ll_clock_logic.h"

// Include all the menu screens so we can call them from here
#include "logic/menu/ll_menu_start.h"
#include "logic/menu/ll_menu_time.h"
#include "logic/menu/ll_menu_alarm.h"
#include "logic/menu/ll_menu_settings.h"
#include "logic/menu/settings/Il_menu_settings_date.h"
#include "logic/menu/settings/Il_menu_settings_time.h"
#include "logic/menu/settings/ll_menu_settings_alarm.h"
#include "logic/menu/settings/ll_menu_settings_timezone.h"
#include "logic/menu/settings/ll_menu_settings_volume.h"
#include "logic/menu/settings/ll_menu_settings_snooze.h"
#include "logic/menu/settings/ll_menu_settings_summertime.h"
#include "logic/menu/settings/ll_menu_settings_isbeep.h"
#include "logic/menu/settings/ll_menu_settings_mac.h"
#include "logic/menu/settings/editor/ll_menu_settings_alarm_editor.h"
#include "logic/menu/settings/editor/ll_menu_settings_time_editor.h"
#include "logic/menu/settings/editor/ll_menu_settings_date_editor.h"

#include "logic/menu/ll_menu_ipr.h"
#include "logic/menu/IPR/ll_menu_IPR_dino_game.h"
#include "logic/menu/IPR/ll_menu_IPR_dino_game_play.h"
#include "logic/menu/IPR/ll_menu_IPR_sleep_schedule.h"
#include "logic/menu/IPR/ll_menu_IPR_sleep_schedule_view.h"
#include "logic/menu/IPR/ll_menu_IPR_openweather.h"
#include "logic/menu/IPR/ll_menu_IPR_celebration_day.h"
#include "logic/menu/IPR/ll_menu_IPR_RSS_feed.h"

void handleMenu(int, int);
void check_for_update(int);

void add_item(menu_item);

static menu_item *menu;

// defines if alarm overrides normal button functions
static unsigned int alarm_override = 0;

static unsigned int currentItem = 0; /* Start at first menu item */
static u_char currentId;
static int time_to_refresh = 0;
static unsigned long alarm_timer = 0;

void ll_menu_init()
{
	menu = malloc(MAX_MENU_ITEMS * sizeof(menu_item));

	// Create menu
	currentId = 0;

	add_item(ll_menu_start_init());
	add_item(ll_menu_time_init());
	add_item(ll_menu_alarm_init());
	//SETTINGS
	add_item(ll_menu_settings_init());
	add_item(ll_menu_settings_date_init());
	add_item(ll_menu_settings_time_init());
	add_item(ll_menu_settings_alarm_init());
	add_item(ll_menu_settings_timezone_init());
	add_item(ll_menu_settings_volume_init());
	//add_item(ll_menu_settings_snooze_init());
	add_item(ll_menu_settings_summertime_init());
	add_item(ll_menu_settings_isbeep_init());
	//add_item(ll_menu_settings_mac_init());

	// EDITOR
	add_item(ll_menu_settings_alarm_editor_init());
	add_item(ll_menu_settings_time_editor_init());
	add_item(ll_menu_settings_date_editor_init());

	//IPR
	add_item(ll_menu_ipr_init());
	add_item(ll_menu_ipr_dino_game_init());
	add_item(ll_menu_ipr_dino_game_play_init());
	add_item(ll_menu_ipr_celebration_init());
	add_item(ll_menu_ipr_openweather_init());
	add_item(ll_menu_ipr_rss_init());
	add_item(ll_menu_ipr_sleep_schedule_init());
	add_item(ll_menu_ipr_sleep_schedule_view_init());

	// Draw first menu item
	printf("done init\n");
	currentId = menu[currentItem].id;
	menu[currentItem].fp_draw();
	printf("done draw\n");

	currentItem = MENU_TIME;
}

void add_item(menu_item item)
{
	menu[currentId] = item;
	currentId++;
}

void ll_menu_update(int tick_rate)
{
	if (alarm_override)
	{
		ll_menu_time_draw();	//update time
		dl_display_backlight_on();	//update backlight
		alarm_timer += tick_rate;
		//alarm overrides menu keys.
		if (KbGetKey() == KEY_OK)
		{
			printf("OK button pressed disabling alarm...");
			untrigger_all_alarms();
			//stop_snooze_alarm_V2(get_triggered_alarm_V2());
			
			alarm_timer = 0;
			//untrigger_alarm();	//get alarm so it can be disabled.
		}

		time_to_refresh += tick_rate;
		check_for_update(tick_rate);
		return;
	}

	int key = get_currently_pressed_key(get_backlight_status());

	if (key == KEYBOARD_LCD_BACKLIGHT_ON)
	{
		dl_display_backlight_on();
	}
	else if (key != 0)
	{
		dl_display_backlight_on();
		handleMenu(key, tick_rate);
	}

	check_for_update(tick_rate);
}

//toggles the override status and returns the current status as an int
int menu_toggle_alarm_override()
{
	printf("\n--ALARM OVERRIDE TOGGLED!--\n");
	if (alarm_override == 0)
	{
		alarm_override = 1;
		return 1;
	}
	else
	{
		alarm_override = 0;
		return 0;
	}
}

void handleMenu(int key, int tick_rate)
{
	/* Switch hardware key to menu key */
	int menu_key = -1;
	switch (key)
	{
	// Menu buttons
	case KEY_ESC:
		menu_key = MENU_KEY_ESC;
		break;
	case KEY_OK:
		menu_key = MENU_KEY_OK;
		break;
	case KEY_UP:
		menu_key = MENU_KEY_UP;
		break;
	case KEY_DOWN:
		menu_key = MENU_KEY_DOWN;
		break;
	case KEY_LEFT:
		menu_key = MENU_KEY_LEFT;
		break;
	case KEY_RIGHT:
		menu_key = MENU_KEY_RIGHT;
		break;
	// Radio buttons
	case KEY_01:
		ll_inet_play_station(1);
		break;
	case KEY_02:
		ll_inet_play_station(2);
		break;
	case KEY_03:
		ll_inet_play_station(3);
		break;
	case KEY_04:
		ll_inet_play_station(4);
		break;
	case KEY_05:
		ll_inet_play_station(5);
		break;
	case KEY_ALT:
		ll_inet_stop_station();
		break;
	// Return to filter non-menu buttons
	default:
		return;
	}

	/* Call onkey and onexit methods */
	if (NULL != menu[currentItem].fp_on_key[menu_key])
	{
		(*menu[currentItem].fp_on_key[menu_key])();
	}

	/* Filter out when new menu id is MENU_NULL (no menu change) */
	if (menu[currentItem].new_id[menu_key] == MENU_NULL)
	{
		return;
	}
	if (NULL != menu[currentItem].fp_on_exit)
		(*menu[currentItem].fp_on_exit)();
	currentId = menu[currentItem].new_id[menu_key];

	/* Lookup new current menu id in the array of menu items */
	currentItem = 0;
	while (menu[currentItem].id != currentId)
		currentItem += 1;

	/* Update the time to refresh */
	if (menu[currentItem].refresh_rate != 0)
		time_to_refresh = menu[currentItem].refresh_rate;

	/* Callentry function */
	if (NULL != menu[currentItem].fp_on_entry)
		(*menu[currentItem].fp_on_entry)();

	/* Display the menu item text */
	menu[currentItem].fp_draw();
}

void check_for_update(int tick_rate)
{
	if (menu[currentItem].refresh_rate == 0)
		return;

	time_to_refresh -= tick_rate;
	if (time_to_refresh <= 0)
	{
		time_to_refresh = menu[currentItem].refresh_rate;
		menu[currentItem].fp_draw();
	}
}

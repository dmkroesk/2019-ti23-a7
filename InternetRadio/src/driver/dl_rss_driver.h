/*
 * dl_rss_driver.h
 *
 * Created: 21-Mar-19 9:50:08 AM
 *  Author: Gerben
 */ 

//#define MENU_IPR_RSS_FEED RSS_MENU

#ifndef RSS_H
#define RSS_H

#include <stdio.h>
#include "logic/menu/ll_data_menu.h"

void test_rss(void);

//FILE *rss_connect_to_server(char *ip, const int port, const char *route);
//void handle_rss_input(menu_item *menu, int input);
char* get_text(void);

void getSubString(char *source, int from, int to);
int getStringBetweenDelimiters(const char *json, const char *leftDelimiter, const char *rightDelimiter, char **output);
//void remove_garbage(char *value, char *output);
void remove_garbage_rss(char *value);

#endif

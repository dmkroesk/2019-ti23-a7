/*
* dl_StorageDriver.c
*
* Created: 19-Feb-19 1:43:53 PM
*  Author: Patrick
*/

/*
* Used <string.h> instead of <stdlib.h> for strcpy.
* Because otherwise you will get warnings
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <fs/fs.h>
#include <fs/uromfs.h>
#include <dev/urom.h>
#include <io.h>

#include <sys/timer.h>
#include "dl_storage_driver.h"
#include "hardware/hl_flash.h"

/*
* Initialize the flash Storage run this method first
* \param void
* \return 0 if initialize went well and -1 if it failed;
*/
int init_flash_storage(void)
{
	return At45dbInit();
}

/*
* Initialize the flash Storage run this method first
* \param void
* \return the settings struct
*/
void initialize_predef_settings(settings *setting)
{
	printf("[Storage] - Initialize predefined Settings");
	strcpy(setting->date, "01-03-2019");
	strcpy(setting->time, "12:31:00");
	int i = 0;
	for (; i < DAY_COUNT; i++)
	{
		setting->alarm[i].weekday = i;
		setting->alarm[i].hour_begin = 17;
		setting->alarm[i].minute_begin = 50;
		setting->alarm[i].hour_end = 17;
		setting->alarm[i].minute_end = 55;
		setting->alarm[i].isTriggered = 0;
		setting->alarm[i].snooze = 0;
	}

	setting->time_zone = 1;
	setting->volume = 5;
	setting->snooze_time = 3;
	setting->summer_time = 0;
	strcpy(setting->mac_address, "F8-34-41-EA-C2-4D");
	setting->is_beep = 0;
	setting->dino_highscore = 0;
}

/*
* Log the settings that we have now
* \param the setting struct you want to print
* \return void
*/
void show_current_setting(settings *struct_setting)
{
	printf("=======SETTING=======\nDate: %s\nTime: %s\nTime zone: %i\nVolume: %i\nSnooze: %i\nSummer Time: %i\nMAC-Address: %s\nBeep: %i\nDino-Score: %i\n",
		   struct_setting->date,
		   struct_setting->time,
		   struct_setting->time_zone,
		   struct_setting->volume,
		   struct_setting->snooze_time,
		   struct_setting->summer_time,
		   struct_setting->mac_address,
		   struct_setting->is_beep,
		   struct_setting->dino_highscore);
	int i = 0;
	printf("=======ALARM=======\n");
	for (; i < DAY_COUNT; i++)
	{
		printf("Day: %i, Begin: %i:%i - End: %i %i - %i\n", struct_setting->alarm[i].weekday, struct_setting->alarm[i].hour_begin, struct_setting->alarm[i].minute_begin, struct_setting->alarm[i].hour_end, struct_setting->alarm[i].minute_end, struct_setting->alarm[i].isTriggered);
	}
}

/*
 * Writes the predefined settings too the flash memory and erases the old settings
 */
int factory_reset(void)
{
	settings *setting = (settings *)malloc(sizeof(settings));
	initialize_predef_settings(setting);
	write_setting(setting, sizeof(settings));
	free(setting);
	return -1;
}
/*
 * Writes the settings struct to the flash memory
*/
int write_setting(settings *setting_struct, unsigned int size)
{
	printf("[Storage] Writing the settings to the flash storage!\n");
	int result = 0;
	unsigned char *storage = (unsigned char *)malloc(sizeof(unsigned char) * size);
	if (storage != NULL)
	{
		memcpy((unsigned char *)storage, setting_struct, size);
		At45dbPageWrite(LOC_PRIORITY_HIGH, (unsigned char *)storage, size);
	}
	free(storage);
	return result;
}

/*
 * Reads the settings struct from the flash memory
*/
int read_setting(settings *setting_struct, unsigned int size)
{
	//printf("[Storage] Reading the settings from the flash storage!\n");
	int result = -1;

	unsigned char *storage = (unsigned char *)malloc(sizeof(unsigned char) * size);
	if (storage != NULL)
	{
		At45dbPageRead(LOC_PRIORITY_HIGH, (unsigned char *)storage, size);
		memcpy((settings *)setting_struct, (unsigned char *)storage, size);
		result = 1;
	}
	free(storage);
	return result;
}

/*
 * dl_InetDriver.h
 *
 * Created: 19-Feb-19 1:43:09 PM
 *  Author: Matthijs & Daan
 */

#ifndef DL_INET_DRIVER_H_
#define DL_INET_DRIVER_H_

#define OK 0
#define FAIL -1

int dl_inet_init(void);
int dl_inet_get_time(void);
int dl_inet_stream_connect(int, int *);
void dl_inet_stream_play(void);
void dl_inet_stream_stop(void);
int retrieve_weather(char **);
int retrieve_holiday(char **ptrjson);
int retrieve_rss(char **);

#endif /* DL_INET_DRIVER_H_ */

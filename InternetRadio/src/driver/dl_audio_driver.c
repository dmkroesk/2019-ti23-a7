/*
 * dl_AudioDriver.c
 *
 * Created: 19-Feb-19 1:43:27 PM
 *  Author: Marck Matthijs & Gerben
 */

#define LOG_MODULE LOG_AUDIO_MODULE

#include "hardware/hl_display.h"
#include "driver/dl_audio_driver.h"
#include "driver/dl_display_driver.h"
#include "logic/menu/ll_data_menu.h"
#include "logic/menu/ll_menu_time.h"

#include <stdio.h>
#include <hardware/hl_vs10xx.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <stdint.h>

int *alarm_thread_running = NULL;

THREAD(alarm_thread, arg);

void dl_audio_init(void)
{
	VsPlayerInit();
}

void dl_audio_beep(uint8_t frequency, uint8_t duration)
{
	VsBeep(frequency, duration);
}

//starts the alarm and returns alarm_running int *
int *dl_audio_ignite_the_beep(void)
{
	*alarm_thread_running = 1;
	NutThreadCreate("ALARM", alarm_thread, alarm_thread_running, 512);
	return alarm_thread_running;
}

//inits the alarm
void dl_audio_init_alarm()
{
	alarm_thread_running = malloc(sizeof(int));
}

//stop the alarm thread
void dl_audio_stop_alarm_thread()
{
	*alarm_thread_running = 0;
}

THREAD(alarm_thread, arg)
{
	int frequency = 50;
	int sleep_duration = 995;
	int beep_duration = 7000;
	while (*alarm_thread_running)
	{
		dl_audio_beep(frequency, beep_duration);
		VsBeepStop();
		NutSleep(sleep_duration);
		ll_menu_time_draw();
	}
	printf("\nAlarm thread killed!\n");
	dl_display_backlight_off();
	NutThreadExit();
};

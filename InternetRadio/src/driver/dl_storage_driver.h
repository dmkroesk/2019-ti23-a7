/*
 * dl_StorageDriver.h
 *
 * Created: 19-Feb-19 1:44:00 PM
 *  Author: Patrick
 */

#ifndef DL_STORAGE_DRIVER_H_
#define DL_STORAGE_DRIVER_H_

#include "logic/ll_clock_logic.h"
#define MAX_CHAR 20
#define MAX_CHAR_MAC 100
#define MAX_CHAR_DATE 15
#define MAX_CHAR_TIME 15

// PAGE LOCATIONS
#define LOC_PRIORITY_HIGH 0
#define LOC_PRIORITY_MED 1
#define LOC_PRIORITY_LOW 2

#define DAY_COUNT 7

typedef struct Settings
{
	char date[MAX_CHAR_DATE];
	char time[MAX_CHAR_TIME];
	alarm_struct alarm[7];
	int time_zone;
	unsigned int volume;
	unsigned int snooze_time;
	unsigned int summer_time;
	char mac_address[MAX_CHAR_MAC];
	int is_beep;
	int dino_highscore;
} settings;

extern int init_flash_storage(void);
extern void initialize_predef_settings(settings *setting);
extern void show_current_setting(settings *struct_setting);

extern int erase_flash(void);
extern int factory_reset(void);
extern void print_pages(void);
extern void print_page(unsigned int page_nr);
extern int write_setting(settings *setting_struct, unsigned int size);
extern int read_setting(settings *setting_struct, unsigned int size);
#endif

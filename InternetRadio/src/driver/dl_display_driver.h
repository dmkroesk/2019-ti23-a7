/*
 * dl_DisplayDriver.h
 *
 * Created: 19-Feb-19 1:41:31 PM
 * Author: Daan
 */

#ifndef DL_DISPLAYDRIVER_H_
#define DL_DISPLAYDRIVER_H_

#define COLS 16
#define ROWS 2
#define BACKLIGHT_TIMEOUT 10000

// Public methods
void dl_display_init(void);
void dl_display_clear(void);
void dl_display_update(int);

void dl_display_char(char);
void dl_display_char_pos(char, int, int);
void dl_display_custom_char(int c);
void dl_display_string(char *);
void dl_display_string_pos(char *, int, int);
void dl_display_multiline(char *, char *);

void dl_display_backlight_on(void);
void dl_display_backlight_off(void);
void resettimer(int);
void create_thread(int *);
void set_timer_value(int);

int get_backlight_status(void);

#endif /* DL_DISPLAYDRIVER_H_ */

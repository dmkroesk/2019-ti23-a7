/*
 * dl_RTCDriver.h
 *
 * Created: 19-Feb-19 1:42:20 PM
 *  Author: Gerben
 */

#ifndef DL_RTCDRIVER_H_
#define DL_RTCDRIVER_H_

#include <time.h>

// Public methods
extern void dl_rtc_init(void);
extern void dl_rtc_get_clock(tm *tm);
extern void dl_rtc_set_clock(tm *tm);
extern void dl_rtc_get_alarm(int index, tm *tm, int *aflgs);
extern void dl_rtc_set_alarm(int index, tm *tm, int aflgs);
extern void dl_rtc_get_status(u_long *sflgs);
extern void dl_rtc_clear_status(u_long sflgs);
extern void dl_rtc_read_regs(u_char addres, u_char *buff, size_t len);
extern void dl_rtc_write(int nv, const u_char *buff, size_t len);

extern void dl_set_clock_y(int year);
extern void dl_set_clock_y_m(int year, int month);
extern void dl_set_clock_y_m_d(int year, int month, int day);
extern void dl_set_clock_y_m_d_h(int year, int month, int day, int hour);
extern void dl_set_clock_y_m_d_h_m(int year, int month, int day, int hour, int minute);
extern void dl_set_clock_y_m_d_h_m_s(int year, int month, int day, int hour, int minute, int second);
extern void dl_set_clock(int year, int month, int day, int hour, int minute, int second, int weekday);
extern void dl_set_daylight_savings_time(int dst);
extern void dl_create_time_array(char *pointer_date, char *pointer_time, int year, int month, int day, int hour, int minute, int second);
extern void dl_create_time_array_struct(char *pointer_date, char *pointer_time, tm curr_time);
extern void dl_rtc_get_current_time(char *pointer_date, char *pointer_time);

extern int dl_rtc_compare_time_structs(tm *first_time, tm *second_time);
extern int dl_rtc_compare_rtc_time(tm *time_tocompare);
extern int dl_rtc_synchtime(tm *net_time);
extern void dl_rtc_set_local_timezone(int new_local_timezone);

extern void dl_rtc_shift_to_local(tm *time);
extern int dl_calc_difference(void);
extern void dl_rtc_shift_time(tm *time, int hours_to_shift);
extern void dl_rtc_test(void);

#endif /* DL_RTCDRIVER_H_ */

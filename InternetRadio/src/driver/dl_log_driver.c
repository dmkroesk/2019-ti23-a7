
/*
* dl_log_driver.c
*
* Created: 21-2-2019 10:51:23
*  Author: Matthijs
*/

#define LOG_MODULE LOG_MAIN_MODULE
#include "hardware/hl_log.h"
#include <string.h>
#include <avr/pgmspace.h>

//Types of logs
/////////////////////////////////
//LOG_EMERG
//LOG_ALERT
//LOG_CRIT
//LOG_ERR
//LOG_WARNING
//LOG_NOTICE
//LOG_INFO
//LOG_DEBUG
///////////////////////////////////////

void log_message_info(char *message)
{
	LogMsg_P(LOG_INFO, PSTR("%s"), message);
}
void log_message_error(char *message)
{
	LogMsg_P(LOG_ERR, PSTR("%s"), message);
}
void log_message_warning(char *message)
{
	LogMsg_P(LOG_WARNING, PSTR("%s"), message);
}
void log_integer(int integer)
{
	LogMsg_P(LOG_INFO, PSTR("%d"), integer);
}

void log_message_debug(char *message)
{
	LogMsg_P(LOG_DEBUG, PSTR("%s"), message);
}

/*
 * dl_streamer_driver.h
 *
 * Created: 26-Feb-19 12:43:12
 *  Author: Daan
 */

#ifndef DL_STREAMER_DRIVER_H_
#define DL_STREAMER_DRIVER_H_

#include <sys/nutconfig.h>
#include <sys/types.h>
#include <stdio.h>
#include <io.h>

void dl_streamer_driver_play(FILE *);
void dl_streamer_driver_stop(void);

#endif /* DL_STREAMER_DRIVER_H_ */

/*
 * dl_KeyboardDriver.h
 *  Author: Matthijs Droogendijk
 */

#ifndef DL_KEYBOARD_DRIVER_H_
#define DL_KEYBOARD_DRIVER_H_

#define KEYBOARD_LCD_BACKLIGHT_ON -1 //tells menulogic that backlight should be on.
void dl_keyboard_init(void);
int get_currently_pressed_key(int lcd_backlight_status);

#endif /* DL_KEYBOARD_DRIVER_H_ */

/*
* dl_streamer_driver.c
*
* Created: 26-Feb-19 12:42:58
*  Author: Daan
*/

#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "driver/dl_streamer_driver.h"
#include "driver/dl_inet_driver.h"
#include "driver/dl_storage_driver.h"
#include "hardware/hl_vs10xx.h"

const int BUFFER_SIZE = 8192;
int run_stream = 0;

THREAD(stream_player_thread, arg);
void check_volume(int *);

void dl_streamer_driver_play(FILE *stream)
{
	if (run_stream == 1)
		return;
	run_stream = 1;
	NutThreadCreate("streamer", stream_player_thread, stream, 512);
	printf("Play thread created. Device is playing stream now !\n");
}

void dl_streamer_driver_stop()
{
	run_stream = 0;
}

THREAD(stream_player_thread, arg)
{
	FILE *stream = (FILE *)arg;
	size_t rbytes = 0;
	char *mp3buf;
	int result = FAIL;
	int nrBytesRead = 0;
	unsigned char iflag;
	int current_volume = 20;
	int counter = 0;
	
	// Init MP3 buffer. NutSegBuf is een globale, systeem buffer
	if (0 != NutSegBufInit(BUFFER_SIZE))
	{
		// Reset global buffer
		iflag = VsPlayerInterrupts(0);
		NutSegBufReset();
		VsPlayerInterrupts(iflag);

		result = OK;
	}

	// Init the Vs1003b hardware
	if (OK == result && -1 == VsPlayerInit() && -1 == VsPlayerReset(0))
	{
		result = FAIL;
	}

	while (1)
	{
		if(run_stream == 0)
			break;
		
		counter++;
		//printf("counter: %d\n", counter);
		if(counter > 20)
		{
			counter = counter % 20;
			check_volume(&current_volume);
		}

		// Query number of byte available in MP3 buffer.
		iflag = VsPlayerInterrupts(0);
		mp3buf = NutSegBufWriteRequest(&rbytes);
		VsPlayerInterrupts(iflag);

		// Bij de eerste keer: als player niet draait maak player wakker (kickit)
		if (VS_STATUS_RUNNING != VsGetStatus())
		{
			if (rbytes < 1024)
			{
				printf("VsPlayerKick()\n");
				VsPlayerKick();
			}
		}

		while (rbytes)
		{
			if(run_stream == 0)
			break;
			
			// Copy rbytes (van 1 byte) van stream naar mp3buf.
			nrBytesRead = fread(mp3buf, 1, rbytes, stream);

			if (nrBytesRead > 0)
			{
				iflag = VsPlayerInterrupts(0);
				mp3buf = NutSegBufWriteCommit(nrBytesRead);
				VsPlayerInterrupts(iflag);
				if (nrBytesRead < rbytes && nrBytesRead < 512)
				{
					NutSleep(250);
				}
			}
			else
			{
				break;
			}
			rbytes -= nrBytesRead;

			if (nrBytesRead <= 0)
			{
				break;
			}
		}
	}

	printf("killing the player\n");
	VsPlayerStop();
	fclose(stream);
	NutThreadExit();
}

void check_volume(int *current_volume)
{
	settings *setting = (settings *)malloc(sizeof(settings));
	read_setting(setting, sizeof(settings));
	int saved_volume = setting->volume;
	free(setting);
	
	if (saved_volume == *current_volume)
		return;
	
	*current_volume = saved_volume;

	u_char volumeToSet = 256.0 - ((saved_volume / 20.0) * 128.0 + 128.0);
    VsSetVolume(volumeToSet, volumeToSet);
    //printf("- VS_volume level: %d, %d\n", saved_volume, volumeToSet);
}

/*
* dl_KeyboardDriver.c
*  Author: Matthijs Droogendijk
*/
#include <stdio.h>
#include <string.h>

#include "hardware/hl_keyboard.h"
#include "driver/dl_keyboard_driver.h"
#include "driver/dl_display_driver.h"

unsigned int previous_key = -2;
volatile int thread_running = 0; //0 for false, 1 for true

void dl_keyboard_init()
{
	KbInit();
	KbScan();
}

int get_currently_pressed_key(int disp_status)
{
	int temp = KbGetKey();
	if (temp == KEY_UNDEFINED)
	{
		//if no key is pressed, return 0
		previous_key = temp;
		return 0;
	}
	if (temp == previous_key)
	{
		// previous didn't change so 0 is returned.
		return 0;
	}
	if (disp_status == 0) //and a button is pressed
	{
		//if backlight is off and previous key is not equal to temporary key.
		previous_key = temp;
		return KEYBOARD_LCD_BACKLIGHT_ON; //menu logic enables backlight
	}
	previous_key = temp;
	return previous_key;
}

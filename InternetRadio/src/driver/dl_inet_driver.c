/*
 * dl_InetDriver.c
 *
 * Created: 19-Feb-19 1:42:57 PM
 *  Author: Matthijs & Daan
 */

#include <sys/socket.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>

#include <pro/sntp.h>
#include <netdb.h>

#include "driver/dl_inet_driver.h"
#include "driver/dl_streamer_driver.h"
#include "driver/dl_log_driver.h"
#include "driver/dl_rtc_driver.h"

#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <string.h>
#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <sys/socket.h>
#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/timer.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_STATIONS 5

#define ETH0_BASE 0xC300
#define ETH0_IRQ 5

TCPSOCKET *sock;
FILE *stream;
char buff[1024];
char *json_pointer;
char tempfromjson[20];
WeatherShouldUpdate = 1;
typedef struct
{
	char *url;
	char *ip;
	int port;
} station;

const station stations[MAX_STATIONS] =
	{
		{// Efteling
		 "/TLPSTR07.mp3",
		 "20863.live.streamtheworld.com",
		 80},
		{// Country
		 "/stream",
		 "91.121.82.33",
		 63224},
		{// Aardschok
		 "/",
		 "as192.pinguinradio.com",
		 80},
		// 	{ // Jazz
		// 		"/stream",
		// 		"199.180.75.116",
		// 		80
		// 	},
		{// Classic Rock
		 "/stream",
		 "212.30.80.195",
		 9034},
		{// 538
		 "/RADIO538.mp3",
		 "18973.live.streamtheworld.com",
		 80},
};

static char eth0IfName[9] = "eth0";
FILE *stream;

char *dl_inet_get_address(int);

int dl_inet_init(void)
{
	uint8_t mac_addr[6] = {0x00, 0x06, 0x98, 0x30, 0x22, 0x66};

	int result = OK;

	// Registreer NIC device (located in nictrl.h)
	printf("initing inet driver...\n");
	result = NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ);
	printf("registered with result %d\n", result);
	if (result)
	{
		log_message_error("Error: >> NutRegisterDevice()");
		result = FAIL;
	}

	if (OK == result)
	{
		log_message_info("Getting IP address (DHCP)");
		if (NutDhcpIfConfig(eth0IfName, NULL, 10000))
		{
			log_message_error("Error: no stored MAC address, try hardcoded MAC");
			if (NutDhcpIfConfig(eth0IfName, mac_addr, 10000))
			{
				log_message_error("Error: >> NutDhcpIfConfig()");
				result = FAIL;
			}
		}
	}

	if (OK == result)
	{
		log_message_info("Networking setup OK, new settings are:\n");

		printf("if_name: %s\n", confnet.cd_name);
		printf("ip-addr: %s\n", inet_ntoa(confnet.cdn_ip_addr));
		printf("ip-mask: %s\n", inet_ntoa(confnet.cdn_ip_mask));
		printf("gw     : %s\n", inet_ntoa(confnet.cdn_gateway));
	}

	return result;
}

int dl_inet_get_time(void)
{
	/* Retrieve NTP time */
	/* [LiHa] Bron: http://www.ethernut.de/nutwiki/index.php/Network_Time_Protocol */
	printf("\nRetrieving time from 1.nl.pool.ntp.org...");
	//char *server = inet_ntoa(NutDnsGetHostByName((u_char *) "1.nl.pool.ntp.org"));
	//printf("server ip: %s\n", server);
	u_long timeserver = inet_addr("5.39.184.5"); /* [LiHa] IP address may change frequently (it's a pool afterall) */
	time_t ntp_time;
	tm *ntp_datetime;
	if (NutSNTPGetTime(&timeserver, &ntp_time) != 0)
	{
		printf("Failed to retrieve time\n");
		return 0;
	}
	else
	{
		ntp_datetime = localtime(&ntp_time);
		dl_rtc_synchtime(ntp_datetime);
		printf("NTP time is: %02d:%02d:%02d\n", ntp_datetime->tm_hour, ntp_datetime->tm_min, ntp_datetime->tm_sec);
		return 1;
	}
}

char *dl_inet_get_address(int stream_index)
{
	char *address = (char *)stations[stream_index].ip;
	printf("return address is %s\n", address);
	int i;
	for (i = 0; i < 4; i++)
		if (address[i] == '.')
			return address;

	u_long return_addr = NutDnsGetHostByName((u_char *)stations[stream_index].ip);
	return inet_ntoa(return_addr);
}

int dl_inet_stream_connect(int stream_index, int *starting_stream)
{
	printf("VAR = %d\n", *starting_stream);
	stream_index--; // index instead of number
	char *address = dl_inet_get_address(stream_index);
	printf("address is %s\n", address);

	char *data;

	TCPSOCKET *socket;

	socket = NutTcpCreateSocket();
	if (NutTcpConnect(socket, inet_addr(address), stations[stream_index].port))
	{
		log_message_error("Error: >> NutTcpConnect()");
		return -1;
	}

	// Check if the stream needs to be closed
	if (*starting_stream != 1)
	{
		return -1;
	}

	stream = _fdopen((int)socket, "r+b");

	log_message_info("writing request to stream...\n");

	// HTTP request
	fprintf(stream, "GET %s HTTP/1.0\r\n", stations[stream_index].url);
	fprintf(stream, "Host: %s\r\n", "62.212.132.54");
	fprintf(stream, "Host: %lu\r\n", inet_addr(stations[stream_index].ip));
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Icy-MetaData: 1\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	log_message_info("request written to stream\n");

	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *)malloc(512 * sizeof(char));

	while (fgets(data, 512, stream))
	{
		// Check if the stream needs to be closed
		if (*starting_stream != 1)
		{
			free(data);
			printf("exited the loop...\n");
			return -1;
		}

		if (0 == *data)
			break;

		//printf("%s", data);
	}

	free(data);
	return 0;
}

void dl_inet_stream_play(void)
{
	log_message_info("sending command to play the stream...\n");
	dl_streamer_driver_play(stream);
}

void dl_inet_stream_stop(void)
{
	log_message_info("sending command to stop the stream...\n");
	dl_streamer_driver_stop();
}

int retrieve_rss(char **rss_json) {
	
	/* Initialisation of TCP sockets with options. */
	int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;

	TCPSOCKET *tcpSocket = NutTcpCreateSocket();

	NutTcpSetSockOpt(tcpSocket, 0x02, &segmentation, sizeof(segmentation));
	NutTcpSetSockOpt(tcpSocket, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));
	NutTcpSetSockOpt(tcpSocket, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));

	//IP: 167.114.118.40 on port 80
	int connected = NutTcpConnect(tcpSocket, inet_addr("167.114.118.40"), 80);

	stream = _fdopen((int)tcpSocket, "r+b");

	//complete link: https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=82a1e92a29e746c3ade0f1b2e2ad5ccd&pageSize=1

	fprintf(stream, "GET %s HTTP/1.1\r\n", "/v2/top-headlines?sources=techcrunch&apiKey=82a1e92a29e746c3ade0f1b2e2ad5ccd&pageSize=1");
	fprintf(stream, "Host: %s\r\n", "newsapi.org");
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: /\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	const char search[1] = "{";
	const char temp[2000];

	/* Looks through buffer until it finds the Json and points towards it */
	while (fgets(buff, sizeof(buff), stream))
	{
		puts(buff);
		*rss_json = strstr(buff, search);

		//printf("rss_json: %s\r\n", *rss_json);
		if (*rss_json != NULL)
		{
			//printf("Found it!: %s\r\n", *rss_json);
			return 1;
		}
		else {
			//printf("Not found!\r\n");
		}
		//printf("Buffer: %s\r\n", buff);
	}
}

int retrieve_weather(char **ptrjson)
{
	/* Initialisation of TCP sockets with options. */
	int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;

	TCPSOCKET *tcpSocket = NutTcpCreateSocket();

	NutTcpSetSockOpt(tcpSocket, 0x02, &segmentation, sizeof(segmentation));
	NutTcpSetSockOpt(tcpSocket, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));
	NutTcpSetSockOpt(tcpSocket, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));

	int connected = NutTcpConnect(tcpSocket, inet_addr("79.170.88.194"), 80);

	stream = _fdopen((int)tcpSocket, "r+b");

	fprintf(stream, "GET %s HTTP/1.1\r\n", "/api/json-data-10min.php?key=12d6c13fb3&locatie=Breda");
	fprintf(stream, "Host: %s\r\n", "weerlive.nl");
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: /\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	const char search[1] = "{";
	const char temp[2000];

	/* Looks through buffer until it finds the Json and points towards it */
	while (fgets(buff, sizeof(buff), stream))
	{
		puts(buff);
		*ptrjson = strstr(buff, search);

		if (*ptrjson != NULL)
		{
			printf("Found it!: %s\n", *ptrjson);
			return 1;
		}
	}
}

int retrieve_holiday(char **ptrjson)
{

	/* Initialisation of TCP sockets with options. */
	int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;

	TCPSOCKET *tcpSocket = NutTcpCreateSocket();

	NutTcpSetSockOpt(tcpSocket, 0x02, &segmentation, sizeof(segmentation));
	NutTcpSetSockOpt(tcpSocket, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));
	NutTcpSetSockOpt(tcpSocket, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));

	int connected = NutTcpConnect(tcpSocket, inet_addr("104.31.12.58"), 80);

	stream = _fdopen((int)tcpSocket, "r+b");
	///v1/:countryCode/:timezone=/today
	fprintf(stream, "GET %s HTTP/1.1\r\n", "/jokes/random");
	printf("=====stream : %s\n", stream);
	fprintf(stream, "Host: %s\r\n", "api.chucknorris.io");
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: /\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	const char search[1] = "{";
	const char temp[2000];

	/* Looks through buffer until it finds the Json and points towards it */
	while (fgets(buff, sizeof(buff), stream))
	{
		puts(buff);
		*ptrjson = strstr(buff, search);

		if (*ptrjson != NULL)
		{
			printf("Found it!: %s\n", *ptrjson);
			return 1;
		}
	}
}

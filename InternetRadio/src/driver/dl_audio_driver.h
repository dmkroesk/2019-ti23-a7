/*
 * dl_AudioDriver.h
 *
 * Created: 19-Feb-19 1:43:34 PM
 *  Author: Gerben
 */
#ifndef HL_AUDIO_DRIVER_H_
#define HL_AUDIO_DRIVER_H_

#include <stdint.h>

extern void dl_audio_init(void);
extern int *dl_audio_ignite_the_beep(void);
extern void dl_audio_beep(uint8_t frequency, uint8_t duration);
extern void dl_audio_init(void);
extern void dl_audio_init_alarm(void);
extern void dl_audio_stop_alarm_thread(void);
#endif /*  HL_AUDIO_DRIVER_H_ */


/*
* dl_log_driver.h
*
* Created: 21-2-2019 10:57:14
*  Author: Matthijs Droogendijk
*/
#ifndef DL_LOG_DRIVER_H_
#define DL_LOG_DRIVER_H_

extern void log_message_info(char *message);
extern void log_message_error(char *message);
extern void log_message_warning(char *message);
extern void log_message_debug(char *message);
extern void log_integer(int integer);

#endif /* DL_LOG_DRIVER_H_ */

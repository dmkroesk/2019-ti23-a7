/*
 * dl_DisplayDriver.c
 *
 * Created: 19-Feb-19 1:41:24 PM
 *  Author: Daan
 */

#define LOG_MODULE LOG_MAIN_MODULE

#include <stdio.h>
#include <time.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/event.h>

#include "hardware/hl_display.h"
#include "driver/dl_log_driver.h"
#include "dl_display_driver.h"
#include "dl_rtc_driver.h"

// Charset holds the custom characters that can be displayed (static for now).
static unsigned char charset[] =
	{
		// Menu custom characters
		0b01110, 0b10001, 0b00100, 0b01010, 0b00000, 0b00100, 0b01110, 0b00100, // Wi-Fi symbol
		0b00000, 0b00000, 0b00000, 0b00000, 0b10001, 0b01010, 0b00100, 0b00000, // Arrow
		// Dino game custom characters
	  	0b00110, 0b00111, 0b00111, 0b10100, 0b11111, 0b11110, 0b01010, 0b01000, // Dino 1
	  	0b00110, 0b00111, 0b00111, 0b10100, 0b11111, 0b11110, 0b01010, 0b00010, // Dino 2
	  	0b00000, 0b00000, 0b00011, 0b00111, 0b10110, 0b11111, 0b01110, 0b01010, // Dino duck
		0b00100, 0b00101, 0b10101, 0b10101, 0b10111, 0b11100, 0b00100, 0b00100, // Cactus
		0b00000, 0b00100, 0b01100, 0b11110, 0b00111, 0b00111, 0b00110, 0b00100, // Bird 1
		0b00000, 0b00001, 0b00101, 0b01111, 0b11111, 0b00011, 0b00000, 0b00000, // Bird 2
};

int backlight_timer = 0;

// Private methods
static void dl_display_test(void);
static void dl_display_set_cursor(int, int);
static void dl_display_init_custom_chars(void);

//prototypes
int get_current_time(void);
void time_difference(void);

// Init's the lcd display. Needs to be called once at startup.
void dl_display_init()
{
	LcdLowLevelInit();
}

// Update the backlight of the lcd display.
void dl_display_update(int tick_rate)
{
	if (backlight_timer == 0)
		return;

	backlight_timer -= tick_rate;
	if (backlight_timer <= 0)
		dl_display_backlight_off();
}

// Private test function
static void dl_display_test()
{
	dl_display_init_custom_chars();
	LcdBackLight(LCD_BACKLIGHT_ON);
	dl_display_string_pos("Hello!", 0, 3);
	dl_display_string_pos("Bye!", 1, 6);
	NutSleep(1000);
	dl_display_clear();
	NutSleep(1000);
	dl_display_string("Test123");

	int i = 0;
	while (1)
	{
		dl_display_char_pos(0, 0, i);
		NutSleep(1000);
		i++;
		if (i > 15)
			i = 0;
		dl_display_clear();
	}
}

// Clears the display and init's the custom chars again
void dl_display_clear()
{
	LcdWriteByte(WRITE_COMMAND, 0x01);
	dl_display_init_custom_chars();
}

// Displays a char on the cursor
void dl_display_char(char c)
{
	LcdChar(c);
}

// Displays a char on a position of the screen (row 0 (top) - 1 (bottom), col 0 (left) - 15 (right))
void dl_display_char_pos(char c, int row, int col)
{
	dl_display_set_cursor(row, col);
	LcdChar(c);
}

// Displays a custom char on a position of the screen (row 0 (top) - 1 (bottom), col 0 (left) - 15 (right))
void dl_display_custom_char(int c)
{
	dl_display_set_cursor(0, 15);
	LcdChar(c);
}

// Displays a string in the top right corner (0, 0)
void dl_display_string(char *s)
{
	dl_display_string_pos(s, 0, 0);
}

// Displays a string on a position of the screen (row 0 (top) - 1 (bottom), col 0 (left) - 15 (right))
void dl_display_string_pos(char *s, int row, int col)
{
	dl_display_set_cursor(row, col);
	for (; *s; s++)
		dl_display_char(*s);
}

// Displays 2 strings on both of the rows (max 16 chars per row) (the screen gets cleared beforehand)
void dl_display_multiline(char *first_line, char *second_line)
{
	dl_display_clear();
	dl_display_string_pos(first_line, 0, 0);
	dl_display_string_pos(second_line, 1, 0);
}

// Set the cursor on a point on the screen (row 0 (top) - 1 (bottom), col 0 (left) - 15 (right))
static void dl_display_set_cursor(int row, int col)
{
	LcdWriteByte(WRITE_COMMAND, (row * 64) + col + 128);
}

// Initializes the custom chars from the charmap
static void dl_display_init_custom_chars()
{
	LcdWriteByte(WRITE_COMMAND, 0x40); // adres 0 van CG-RAM
	unsigned int sz = sizeof(charset) / sizeof(char);
	int ch;
	for (ch = 0; ch < sz; ch++)
		LcdWriteByte(WRITE_DATA, charset[ch]); // schrijf data in CG-RAM
}

// Turns the backlight on and off again after 10 seconds (needs a fix)
void dl_display_backlight_on()
{
	if (backlight_timer <= 0)
		LcdBackLight(LCD_BACKLIGHT_ON);
	backlight_timer = 10000;
}

void dl_display_backlight_off()
{
	backlight_timer = 0;
	LcdBackLight(LCD_BACKLIGHT_OFF);
}

int get_backlight_status(void)
{
	if (backlight_timer <= 0)
		return LCD_BACKLIGHT_OFF;
	return LCD_BACKLIGHT_ON;
}

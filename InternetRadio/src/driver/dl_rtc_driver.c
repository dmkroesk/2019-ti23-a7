/*
 * dl_RTCDriver.c
 *
 * Created: 19-Feb-19 1:42:14 PM
 *  Author: Gerben
 */
#define LOG_MODULE LOG_RTC_MODULE

#include <sys/timer.h>
#include <time.h>
#include <string.h>
#include <avr/pgmspace.h>

#include "hardware/hl_log.h"
#include "hardware/hl_rtc.h"
#include "driver/dl_display_driver.h"
#include "driver/dl_log_driver.h"
#include "driver/dl_rtc_driver.h"

int _local_timezone;

void dl_rtc_init(void)
{
	X12Init();
	//dl_rtc_test();
	_local_timezone = 1;
}

void dl_rtc_set_local_timezone(int new_local_timezone)
{
	_local_timezone = new_local_timezone;
}

void dl_rtc_get_clock(tm *tm)
{
	X12RtcGetClock(tm);
}

void dl_rtc_set_clock(tm *tm)
{
	X12RtcSetClock(tm);
}

void dl_rtc_get_alarm(int index, tm *tm, int *aflgs)
{
	X12RtcGetAlarm(index, tm, aflgs);
}

void dl_rtc_set_alarm(int index, tm *tm, int aflgs)
{
	X12RtcSetAlarm(index, tm, aflgs);
}

void dl_rtc_get_status(u_long *sflgs)
{
	X12RtcGetStatus(sflgs);
}

void dl_rtc_clear_status(u_long sflgs)
{
	X12RtcClearStatus(sflgs);
}

void dl_rtc_read_regs(u_char addres, u_char *buff, size_t len)
{
	X12RtcReadRegs(addres, buff, len);
}

void dl_rtc_write(int nv, const u_char *buff, size_t len)
{
	X12RtcWrite(nv, buff, len);
}

void dl_rtc_eeprom_read(u_int addr, void *buff, size_t len)
{
	X12EepromRead(addr, buff, len);
}

void dl_rtc_eeprom_write(u_int addr, const void *buff, size_t len)
{
	X12EepromWrite(addr, buff, len);
}

void dl_set_clock_y(int year)
{
	dl_set_clock(year, 0, 0, 0, 0, 0, 0);
}

void dl_set_clock_y_m(int year, int month)
{
	dl_set_clock(year, month, 0, 0, 0, 0, 0);
}

void dl_set_clock_y_m_d(int year, int month, int day)
{
	dl_set_clock(year, month, day, 0, 0, 0, 0);
}

void dl_set_clock_y_m_d_h(int year, int month, int day, int hour)
{
	dl_set_clock(year, month, day, hour, 0, 0, 0);
}

void dl_set_clock_y_m_d_h_m(int year, int month, int day, int hour, int minute)
{
	dl_set_clock(year, month, day, hour, minute, 0, 0);
}

void dl_set_clock_y_m_d_h_m_s(int year, int month, int day, int hour, int minute, int second)
{

	dl_set_clock(year, month, day, hour, minute, second, 0);
}

/*
	Description:	Method to manually set the time in the RTC chip
	params:			year:	the current year
					month:	the month				[1-12]
					day:	the day of the month	[1-31]
					hour:	the current hour		[0-23]
					minute: the current minute		[0-59]
					second: the current second		[0-59]
					weekday:the day of the week		[0-6]
*/

void dl_set_clock(int year, int month, int day, int hour, int minute, int second, int weekday)
{
	tm new_time;
	new_time.tm_year = year;
	new_time.tm_mon = month;
	new_time.tm_mday = day;
	new_time.tm_wday = weekday;
	new_time.tm_yday = 0; // dl_calc_day_of_year(month, day);
	new_time.tm_hour = hour;
	new_time.tm_min = minute;
	new_time.tm_sec = second;
	new_time.tm_isdst = 0;

	dl_rtc_set_clock(&new_time);
}

/*
	description: Method to set the DaylightSavingsTime value
	
	param:		 int dst	-> 0 to disable DST
							-> 1 to enable DST
*/
void dl_set_daylight_savings_time(int dst)
{
	tm curr_time;
	dl_rtc_get_clock(&curr_time);
	curr_time.tm_isdst = dst;
	dl_rtc_set_clock(&curr_time);
}

void dl_rtc_get_current_time(char *pointer_date, char *pointer_time)
{
	tm current_time;
	dl_rtc_get_clock(&current_time);
	dl_create_time_array_struct(pointer_date, pointer_time, current_time);
}

void dl_create_time_array_struct(char *pointer_date, char *pointer_time, tm curr_time)
{
	dl_create_time_array(pointer_date, pointer_time, curr_time.tm_year, curr_time.tm_mon, curr_time.tm_mday, curr_time.tm_hour, curr_time.tm_min, curr_time.tm_sec);
}
void dl_create_time_array(char *pointer_date, char *pointer_time, int year, int month, int day, int hour, int minute, int second)
{
	char year_string[5];
	sprintf(year_string, "%04d", (year + 1900));
	char month_string[3];
	sprintf(month_string, "%02d", (month + 1));
	char day_string[3];
	sprintf(day_string, "%02d", day);

	strcpy(pointer_date, day_string);
	strcat(pointer_date, "/");
	strcat(pointer_date, month_string);
	strcat(pointer_date, "/");
	strcat(pointer_date, year_string);

	char hour_string[3];
	sprintf(hour_string, "%02d", hour);
	char minute_string[3];
	sprintf(minute_string, "%02d", minute);
	char seconds_string[3];
	sprintf(seconds_string, "%02d", second);

	strcpy(pointer_time, hour_string);
	strcat(pointer_time, ":");
	strcat(pointer_time, minute_string);
	strcat(pointer_time, ":");
	strcat(pointer_time, seconds_string);
}

int dl_rtc_synchtime(tm *net_time)
{
	dl_rtc_shift_to_local(net_time);
	int synched = 0;
	int same;
	same = dl_rtc_compare_rtc_time(net_time);
	if (same == 0)
	{
		synched = 1;
		dl_rtc_set_clock(net_time);
		log_message_info("Timer liep niet synchroon, geschynchroniseerd!");
	}
	else
	{
		synched = 0;
		log_message_info("Timer loopt bij!");
	}
	return (synched);
}

int dl_rtc_compare_rtc_time(tm *time_tocompare)
{
	tm *chip_time = malloc(sizeof(tm));
	dl_rtc_get_clock(chip_time);

	log_message_info("Old time:");
	int same;
	same = dl_rtc_compare_time_structs(chip_time, time_tocompare);

	free(chip_time);
	return same;
}

int dl_rtc_compare_time_structs(tm *first_time, tm *second_time)
{
	int same = 0;
	if (first_time->tm_year == second_time->tm_year)
	{
		if (first_time->tm_mon == second_time->tm_mon)
		{
			if (first_time->tm_mday == second_time->tm_mday)
			{
				if (first_time->tm_hour == second_time->tm_hour)
				{
					if (first_time->tm_min == second_time->tm_min)
					{
						if (first_time->tm_sec == second_time->tm_sec)
						{
							same = 1;
						}
					}
				}
			}
		}
	}
	return (same);
}

void dl_rtc_shift_to_local(tm *time)
{
	int hours_to_shift = dl_calc_difference();
	dl_rtc_shift_time(time, hours_to_shift);
}

int dl_calc_difference(void)
{
	return (_local_timezone + 5);
}

void dl_rtc_shift_time(tm *time, int hours_to_shift)
{
	int hours = time->tm_hour;
	hours += hours_to_shift;
	time->tm_hour = hours;
}

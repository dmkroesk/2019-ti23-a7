/*
 * dl_rss_driver.c
 *
 * Created: 21-Mar-19 9:49:57 AM
 *  Author: Gerben
 */ 

#include "driver/dl_display_driver.h"
#include "driver/dl_rss_driver.h"
#include "driver/dl_inet_driver.h"
#include "logic/ll_rss_logic.h"
#include "logic/ll_inet_logic.h"
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

char* output_string;

void test_rss() {
    char* json_input = "{    \"status\": \"ok\",    \"totalResults\": 10,    \"articles\": [        {            \"source\": {                \"id\": \"techcrunch\",                \"name\": \"TechCrunch\"            },            \"author\": \"Kirsten Korosec\",            \"title\": \"Tesla's customer referral program is back weeks after it was killed off\", \"description\": \"Tesla killed off its customer referral program way back on Feb. 1, 2019 because the popular program was getting too costly to maintain. But now, less than two months later, Tesla is bringing it back with new incentives aimed at rewarding its customers, and br\u2026\",            \"url\": \"https://techcrunch.com/2019/03/22/teslas-customer-referral-program-is-back-weeks-after-it-was-killed-off/\",            \"urlToImage\": \"https://techcrunch.com/wp-content/uploads/2019/03/20190314_2103201.jpg?w=711\",            \"publishedAt\": \"2019-03-22T07:03:39Z\",            \"content\": \"Tesla killed off its customer referral program way back on Feb. 1, 2019 because the popular program was getting too costly to maintain. But now, less than two months later, Tesla is bringing it back with new incentives aimed at rewarding its customers, and br\u2026 [+1678 chars]\"        }    ]}";
    char *p_json;

    //Check if there is a inet connection first
    printf("Connected? %i", ll_inet_connected());
    if(ll_inet_connected() == 1)
    {
        printf("INIT connected!\r\n");
        retrieve_rss(&p_json);
        if(strlen(p_json) > 0) {
            //printf("Retrieve from json\n");
            printf("JSON INET %s\n", p_json);
            
            if(0 == get_string_between_delimiters(p_json, "title", "description", &output_string)) {
                //printf("Outputstring: %s\n", output_string);
                remove_garbage_rss(output_string);
                //printf("Finalstring: %s\n", output_string);
            }
            
        }
        else {
            //Inet, but json response is not valid
            if(0 == get_string_between_delimiters(json_input, "title", "description", &output_string)) {
                //printf("Outputstring: %s\n", output_string);
                remove_garbage_rss(output_string);
                //printf("Finalstring: %s\n", output_string);
            }
        }
    }

    //No inet, so also use hardcoded JSON
    else {
        printf("No inet, so use hardcoded JSON\n");
        if(0 == get_string_between_delimiters(json_input, "title", "description", &output_string)) {
            //printf("Outputstring: %s\n", output_string);
            remove_garbage_rss(output_string);
            //printf("Finalstring: %s\n", output_string);
        }
    }
}

char* get_text() {
    //printf("Output string: %s", output_string);
    if(NULL != output_string) {
        //printf("GETTEXT() output: %s", output_string);
        return output_string;
    }
    return 0;
}

void remove_garbage_rss(char *value) {
    //printf("Value: %s", value);
    value[strlen(value) - 3] = 0;
    strcpy(output_string, &value[3]);
    //printf("Value after: %s", output_string);
}

int get_string_between_delimiters(const char *json, const char *leftDelimiter, const char *rightDelimiter, char **output) {
    //printf("JSON IN DELIMITER%s\n", json);
    // find the left delimiter and use it as the beginning of the substring
    const char *beginning = strstr(json, leftDelimiter);
    if (beginning == NULL)
        return 1; // left delimiter not found

    //printf("Beginning %s\n", beginning);
    // find the right delimiter`
    const char *end = strstr(json, rightDelimiter);
    if (end == NULL)
        return 2; // right delimiter not found
    //printf("End %s\n", end);
    // offset the beginning by the length of the left delimiter, so beginning points after the left delimiter
    beginning += strlen(leftDelimiter);

    // get the length of the substring
    ptrdiff_t segmentLength = end - beginning;
    //printf("seg length%i\n", segmentLength);
    // allocate memory and copy the substring there
    *output = malloc(segmentLength + 1);
    if (*output == 0)
    {
        printf("BROKEN seg length%i", segmentLength);
    }
    strncpy(*output, beginning, segmentLength);
    (*output)[segmentLength] = 0;
    //printf("OUTPUT %s", *output);
    //printf("lengte: %d, inhoud string: %s", strlen(*output), *output);
    return 0; // success!
}
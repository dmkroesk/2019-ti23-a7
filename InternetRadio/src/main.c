/*
* main.c
*
* Created: 22-Feb-19 10:44:14
*  Author: Daan, Matthijs, Patrick, Marck & Gerben
*/

#define LOG_MODULE LOG_MAIN_MODULE

// Refresh rate of the main menu loop
#define TICK_RATE 50

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <sys/socket.h>

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include "hardware//hl_keyboard.h"
#include "hardware/hl_system.h"
#include "hardware/hl_portio.h"
#include "hardware/hl_display.h"
#include "hardware/hl_remcon.h"
#include "hardware/hl_led.h"
#include "hardware/hl_log.h"
#include "hardware/hl_uart0driver.h"
#include "hardware/hl_mmc.h"
#include "hardware/hl_watchdog.h"
#include "hardware/hl_flash.h"
#include "hardware/hl_spidrv.h"
#include "hardware/hl_rtc.h"

#include "driver/dl_keyboard_driver.h"
#include "driver/dl_log_driver.h"
#include "driver/dl_display_driver.h"
#include "driver/dl_rtc_driver.h"
#include "driver/dl_storage_driver.h"
#include "driver/dl_inet_driver.h"
#include "driver/dl_audio_driver.h"
#include "driver/dl_rss_driver.h"

#include "logic/ll_rss_logic.h"

#include "logic/ll_inet_logic.h"
#include "logic/ll_menu_logic.h"
#include "logic/ll_clock_logic.h"

#include <avr/io.h>
#include <avr/interrupt.h>

static void init(void);
static void update(void);
static void SysMainBeatInterrupt(void *);
static void SysControlMainBeat(u_char);
static void factory_reset_check(void);
static void restart_radio(void);
int ethernet_getRequest2(char* ip, int port, char* route, char* host);

TCPSOCKET *sock;
FILE *stream;
char buff[1024];
char *ret;
char temp2[6] = "temp";

char buff[1024];

int main(void)
{
	init();

	dl_set_clock(119, 0, 1, 0, 0, 59, 0);

	factory_reset_check();
	settings *read_set = (settings *)malloc(sizeof(settings));
	read_setting(read_set, sizeof(settings));
	show_current_setting(read_set);

	while (1)
	{
		NutSleep(TICK_RATE);

		update();

		WatchDogRestart();
	}

	return 0;
}

THREAD(power_button_thread, arg)
{
	while (1)
	{
		if (KbGetKey() == KEY_POWER) {
			restart_radio();
		}
		NutSleep(10);
	}
}

void update()
{
	dl_display_update(TICK_RATE);
	ll_menu_update(TICK_RATE);
	update_alarm(TICK_RATE);
}

void init()
{
	WatchDogDisable();
	NutDelay(100);
	SysInitIO();
	SPIinit();
	LedInit();

	dl_rtc_init();

	Uart0DriverInit();
	Uart0DriverStart();
	LogInit();
	CardInit();
	dl_audio_init();

	X12Init();
	init_flash_storage();
	tm gmt;
	if (X12RtcGetClock(&gmt) == 0)
		LogMsg_P(LOG_INFO, PSTR("RTC time [%02d:%02d:%02d]\n"), gmt.tm_hour, gmt.tm_min, gmt.tm_sec);

	RcInit();
	dl_display_init();
	dl_keyboard_init();
	dl_audio_init();
	dl_audio_init_alarm();
	ll_menu_init();
	ll_inet_init();
	init_alarms();
	//test_rss();
	// char *route = "/v2/everything?q=Apple&from=2019-03-21&sortBy=popularity&apiKey=82a1e92a29e746c3ade0f1b2e2ad5ccd";

    // //printf("%d", rss_connect_to_server("167.114.118.40", 80, route));
	// connect_to_rss("167.114.118.40", 80, route);
	// Init power button check
	NutThreadCreate("power_button", power_button_thread, NULL, 512);

	SysControlMainBeat(ON); // enable 4.4 msecs hartbeat interrupt

	/* Increase our priority so we can feed the watchdog */
	NutThreadSetPriority(1);
	//test_rss();

	/* Enable global interrupts */
	sei();

	//ethernet_getRequest2("167.114.118.40", 80, "/v2/everything?q=Apple&from=2019-03-21&sortBy=popularity&apiKey=40b1cae7c83d4441841b524eb9b069b8", "newsapi.org");
	//ethernet_getRequest("167.114.118.40", 80, "/v2/everything?q=Apple&from=2019-03-21&sortBy=popularity&apiKey=82a1e92a29e746c3ade0f1b2e2ad5ccd", "newsapi.org");


}

int ethernet_getRequest2(char* ip, int port, char* route, char* host) {  
    int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;

    /* Connecting to api */

    sock = NutTcpCreateSocket();
    NutTcpSetSockOpt(sock, 0x02, &segmentation, sizeof(segmentation));

	NutTcpSetSockOpt(sock, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));	

	NutTcpSetSockOpt(sock, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));
	
   	NutTcpConnect(sock, inet_addr(ip), port);

	stream = _fdopen((int) sock, "r+b");

	fprintf(stream, "GET %s HTTP/1.0\r\n", route);
    fprintf(stream, "Host: %s\r\n", host);
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: /\r\n");
    fprintf(stream, "Connection: close\r\n\r\n");
    fflush(stream);


    while (fgets(buff, sizeof(buff), stream)) {
        puts(buff);
    }

    ret = strstr(buff,temp2);

    fclose(stream);
    NutTcpCloseSocket(sock);
    return 1;
}

static void SysMainBeatInterrupt(void *p)
{
	dl_keyboard_init();
	CardCheckCard();
}

void SysInitIO(void)
{
	outp(0xF7, DDRB);
	outp(0x0C, DDRD);
	outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);
	outp(0x8E, DDRE);
	outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

#ifndef USE_JTAG
	sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
	sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256
#endif					//USE_JTAG

	cbi(OCDR, IDRD);
	cbi(OCDR, IDRD);

	outp(0x0E, DDRF);
	outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

	outp(0x18, DDRG);
}

static void SysControlMainBeat(u_char OnOff)
{
	int nError = 0;

	if (OnOff == ON)
	{
		nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
		if (nError == 0)
		{
			init_8_bit_timer();
		}
	}
	else
	{
		// disable overflow interrupt
		disable_8_bit_timer_ovfl_int();
	}
}

static void factory_reset_check(void)
{
	if (KbGetKey() == KEY_ALT)
	{
		printf("Factory Reset!\n");
		factory_reset();
		dl_display_multiline("Factory Reset!", "");
		NutSleep(5000);
	}
}

void restart_radio()
{
	WatchDogEnable();
	WatchDogStart(30);
	while (1)
	{
	}
}
